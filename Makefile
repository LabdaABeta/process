.PHONY: clean default debug release

GPROPTS=-j4 -d -gnatef

default: debug

debug:
	gprbuild $(GPROPTS) -P process.gpr -XMODE=Debug -XPARSER=RD

release:
	gprbuild $(GPROPTS) -P process.gpr -XMODE=Release -XPARSER=RD

clean:
	gprclean -P process.gpr

