# Process

Process is a language designed to be able to represent processes written in
multiple languages. The original design of Process was to describe a build
process from login shell configuration all the way through compilation to
execution. As such it had to describe shell, makefiles, environment, and even
some programming language code.

Process ignores the syntax of values and executions focussing only on the
semantics of what depends on what.

## Grammar

As with any language a grammar is a useful place to start:

### Passable to http://www.bottlecaps.de/rr/ui

```
Process ::= Event*
Event ::= Single_Event | Block_Event
Single_Event ::= (Assignment | Call) ';'
Block_Event ::= Condition | Group
Assignment ::= Variable '=' Expression
Call ::= '@' '<' identifier '>'
Condition ::= '?(' Expression ')' '{' Branch* '}'
Group ::= (Execution | '<' identifier '>')? '{' Process '}'
Variable ::= '(' identifier ')' | '[' identifier ']'
Expression ::= (Variable | Text)*
Execution ::= '%(' Expression ')'
Branch ::= Results Event
Results ::= ':' | (Expression ':')*
Text ::= '"' escaped '"'
/* Any identifier/escaped sequence must \-escape its terminal symbol */
```

### One production per line

LR(0) format:

```
$accept -> Process $end
Process ->
Process -> Process Event
Event -> Variable = Expression ;
Event -> @ < Identifier > ;
Event -> % ( Expression ) { Process }
Event -> ? ( Expression ) { Branches }
Event -> < Identifier > { Process }
Event -> { Process }
Variable -> ( Identifier )
Variable -> [ Identifier ]
Expression -> Expression_Part
Expression -> Expression Expression_Part
Branches ->
Branches -> Branches Branch
Expression_Part -> Variable
Expression_Part -> % ( Expression )
Expression_Part -> q Text q
Branch -> Results Event
Results -> :
Results -> Expression : Results
Identifier -> a
Identifier -> Identifier a
Text ->
Text -> Text s
```

LL(1) format:

```
$accept -> Process $end
Process ->
Process -> Event Process
Event -> Variable = Expression ;
Event -> @ < Identifier > ;
Event -> % ( Expression ) { Process }
Event -> ? ( Expression ) { Branches }
Event -> < Identifier > { Process }
Event -> { Process }
Variable -> ( Identifier )
Variable -> [ Identifier ]
Expression -> Expression_Part More_Expression
More_Expression ->
More_Expression -> Expression_Part Expression
Branches ->
Branches -> Branch Branches
Expression_Part -> Variable
Expression_Part -> % ( Expression )
Expression_Part -> q Text q
Branch -> Results Event
Results -> :
Results -> Expression : Results
Identifier -> a More_Identifier
More_Identifier ->
More_Identifier -> a More_Identifier
Text ->
Text -> s Text
```

### Suitable for Bison

LR(0) format:

```
%%
Process         : | Process Event;
Event           : Variable '=' Expression ';'
                | '@' '<' Identifier '>' ';'
                | '%' '(' Expression ')' '{' Process '}'
                | '?' '(' Expression ')' '{' Branches '}'
                | '<' Identifier '>' '{' Process '}'
                | '{' Process '}';
Variable        : '(' Identifier ')'
                | '[' Identifier ']';
Expression      : Expression_Part | Expression Expression_Part;
Branches        : | Branches Branch;
Expression_Part : Variable
                | '%' '(' Expression ')'
                | 'q' Text 'q';
Branch          : Results Event;
Results         : ':' | Expression ':' Results;
Identifier      : 'a' | Identifier 'a';
Text            : | Text 's';
```

LL(1) format:
```
%%
Process         : | Event Process;
Event           : Variable '=' Expression ';'
                | '@' '<' Identifier '>' ';'
                | '%' '(' Expression ')' '{' Process '}'
                | '?' '(' Expression ')' '{' Branches '}'
                | '<' Identifier '>' '{' Process '}'
                | '{' Process '}';
Variable        : '(' Identifier ')'
                | '[' Identifier ']';
Expression      : Expression_Part More_Expression;
More_Expression : | Expression_Part Expression;
Branches        : | Branch Branches;
Expression_Part : Variable
                | '%' '(' Expression ')'
                | 'q' Text 'q';
Branch          : Results Event;
Results         : ':' | Expression ':' Results;
Identifier      : 'a' More_Identifier;
More_Identifier : | 'a' More_Identifier;
Text            : | 's' Text;
```
