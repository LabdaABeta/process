if exists("b:current_syntax")
    finish
endif

syntax match processOperator "\v\="
syntax match processOperator "\v\@"
syntax match processOperator "\v\{"
syntax match processOperator "\v\}"
syntax match processOperator "\v:"
syntax match processOperator "\v\?\("
syntax match processOperator "\v\%\("
syntax match processOperator "\v\)"
syntax match processComment "\v#.*$"
syntax match processCComment "\v//.*$"
syntax region processBlockName start=/\v</ skip=/\v\\./ end=/\v>/
syntax region processGlobalVariableName start=/\v\(/ skip=/\v\\./ end=/\v\)/
syntax region processLocalVariableName start=/\v\[/ skip=/\v\\./ end=/\v\]/
syntax region processString start=/\v"/ skip=/\v\\./ end=/\v"/

highlight link processOperator Statement
highlight link processCComment Comment
highlight link processComment Comment
highlight link processString Constant
highlight link processBlockName PreProc
highlight link processGlobalVariableName Identifier
highlight link processLocalVariableName Identifier

let b:current_syntax = "process"


" Folding stuff
setlocal foldmethod=expr
setlocal foldexpr=GetProcessFold(v:lnum)

function! GetDentMod(str)
    let opens = substitute(a:str, '[^{(]', '', 'g')
    let closes = substitute(a:str, '[^})]', '', 'g')

    return len(opens) - len(closes)
endfunction

function! GetProcessFold(lnum)
    if getline(a:lnum) =~? '\v^\s*$'
        return '-1'
    endif

    let dentmod=GetDentMod(getline(a:lnum))

    if dentmod > 0
        return 'a' . dentmod
    elseif dentmod < 0
        return 's' . -dentmod
    else
        return '='
    endif
endfunction

