with Ada.Containers;
with Ada.Strings.Hash;
with Graphviz; use Graphviz;
with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Containers.Indefinite_Hashed_Sets;
with Ada.Exceptions;

package body DOTProcess is
    package Node_Maps is new Ada.Containers.Indefinite_Hashed_Maps (
        Key_Type => String,
        Element_Type => Node,
        Hash => Ada.Strings.Hash,
        Equivalent_Keys => "=");

    package Node_Sets is new Ada.Containers.Indefinite_Hashed_Sets (
        Element_Type => Node,
        Hash => Hash,
        Equivalent_Elements => "=");

    function Name_Hash (Name : String) return Colour is
        use type Ada.Containers.Hash_Type;
        Val : Ada.Containers.Hash_Type := Ada.Strings.Hash (Name);
        Hue : Long_Float :=
            Long_Float (Val) / Long_Float (Ada.Containers.Hash_Type'Last);
    begin
        return (
            Kind => HSV,
            H => HSV_Component (Hue),
            S => 0.5, V => 1.0);
    end Name_Hash;

    function Global_Open (Global : Boolean) return String is begin
        if Global then
            return "<b>";
        else
            return "";
        end if;
    end Global_Open;

    function Global_Close (Global : Boolean) return String is begin
        if Global then
            return "</b>";
        else
            return "";
        end if;
    end Global_Close;

    function HTML_Of (
        Expr : Process.Expression;
        Top_Level : Boolean := True)
        return String is
        Next : Process.Expression := null;
        use type Process.Expression;
    begin
        if Expr = null then
            return "";
        end if;

        if not Top_Level then
            Expr.Next (Next);
        end if;

        case Expr.Get.Kind is
            when Process.Variable_Value =>
                return
                    "<i><u>" & Global_Open (Expr.Get.Target.Global) &
                    Expr.Get.Target.Name &
                    Global_Close (Expr.Get.Target.Global) & "</u></i>" &
                    HTML_Of (Next, Top_Level);
            when Process.Text_Value =>
                return Expr.Get.Text & HTML_Of (Next, Top_Level);
            when Process.Execution_Value =>
                declare
                    Child : Process.Expression;
                begin
                    Expr.First (Child);
                    return "&lang;" & HTML_Of (Child, False) & "&rang;"
                        & HTML_Of (Next, Top_Level);
                end;
            when Process.Root_Value =>
                declare
                    Child : Process.Expression;
                begin
                    Expr.First (Child);
                    return HTML_Of (Child, False) & HTML_Of (Next, Top_Level);
                end;
        end case;
    end HTML_Of;

    procedure Ensure_Block (
        After : in out Node_Sets.Set;
        Under : in Graph;
        Item : out Node) is
        function Create_New return Node is
            Self : Node := Create (Under);

            procedure Link_Up (From : in Node_Sets.Cursor) is
            begin
                Set (Create (Node_Sets.Element (From), Self));
            end Link_Up;
        begin
            Set (Create (Item, Self));
            Set_Label (Self,
                "<table align=""center"" " &
                "cellpadding=""0"" " &
                "cellspacing=""0"" " &
                "border=""0""></table>");
            Set_Label_Kind (Self, HTML_LIKE);
            Set_Shape (Self, RECTANGLE);
            Node_Sets.Iterate (After, Link_Up'Access);
            After := Node_Sets.To_Set (Self);
            return Self;
        end Create_New;

        use type Ada.Containers.Count_Type;
    begin
        if Node_Sets.Length (After) /= 1 then
            Item := Create_New;
        else
            Item := Node_Sets.Element (Node_Sets.First (After));

            if Get_Shape (Item) /= RECTANGLE then
                Item := Create_New;
            end if;
        end if;
    end Ensure_Block;

    procedure Append_Assignment (
        After : in out Node_Sets.Set;
        Under : in Graph;
        From : in Process.Incident) is
        Into : Node;
    begin
        Ensure_Block (After, Under, Into);
        declare
            Contents : String := Get_Label (Into);
        begin
            Set_Label (Into,
                Contents (Contents'First .. Contents'Last - 8) &
                "<tr><td align=""right"" valign=""top"">" &
                Global_Open (From.Target.Global) &
                From.Target.Name &
                Global_Close (From.Target.Global) &
                "</td><td align=""center"" valign=""top"">=</td>" &
                "<td align=""left"" valign=""top"">" &
                HTML_Of (From.Value) &
                "</td></tr></table>");
        end;
    end Append_Assignment;

    procedure Append_Call (
        After : in out Node_Sets.Set;
        Under : in Graph;
        From : in Process.Incident;
        Nodes : in Node_Maps.Map) is
        Into : Node;
    begin
        Ensure_Block (After, Under, Into);
        declare
            Contents : String := Get_Label (Into);
            Callee : Node := Node_Maps.Element (Nodes, From.Callee);
            Link : Edge := Create (Into, Callee);
        begin
            Set_Label (Into,
                Contents (Contents'First .. Contents'Last - 8) &
                "<tr><td align=""center"" colspan=""3"" port=""" &
                Get_Id (Callee) & "port" &
                """>" &
                From.Callee &
                "</td></tr></table>");
            Set_From_Port (Link, Get_Id (Callee) & "port");
            Set_Dir (Link, NONE);
        end;
    exception
        when Constraint_Error =>
            Ada.Exceptions.Raise_Exception (Malformed_Error'Identity,
                "Block '" & From.Callee & "' not found!");
    end Append_Call;

    function Create_Condition (
        Into : in Graph;
        From : in Process.Incident)
        return Node is
        Result : Node := Create (Into);
    begin
        Set_Label (Result, HTML_Of (From.Condition));
        Set_Label_Kind (Result, HTML_LIKE);
        Set_Shape (Result, DIAMOND);
        return Result;
    end Create_Condition;

    function Create_Branch (
        Into : in Graph;
        From : in Process.Incident)
        return Node_Sets.Set is
        Result : Node_Sets.Set := Node_Sets.Empty_Set;
        Content : Process.Expression;
        use type Process.Expression;
    begin
        From.Result.First (Content);

        if Content = null then
            declare
                Join : Node := Create (Into);
            begin
                Set_Label (Join, "");
                Set_Shape (Join, POINT);
                Node_Sets.Include (Result, Join);
            end;
        end if;

        while Content /= null loop
            declare
                Value : Node := Create (Into);
            begin
                Set_Label (Value, HTML_Of (Content));
                Set_Label_Kind (Value, HTML_LIKE);
                Set_Shape (Value, ELLIPSE);
                Node_Sets.Include (Result, Value);
            end;
            Content.Next (Content);
        end loop;

        return Result;
    end Create_Branch;

    function Create_Execution (
        Into : in Graph;
        From : in Process.Incident)
        return Node is
        Result : Node := Create (Into);
    begin
        Set_Label (Result, HTML_Of (From.Execution));
        Set_Label_Kind (Result, HTML_LIKE);
        Set_Shape (Result, PLAINTEXT);
        return Result;
    end Create_Execution;

    procedure Generate (
        From : in Process.Processes.Node;
        Into : in out Graph;
        Nodes : in out Node_Maps.Map;
        After : in out Node_Sets.Set) is
        use type Process.Processes.Node;

        procedure Make_Edges (To : in Node) is
            procedure Add_Edge (From : in Node_Sets.Cursor) is
            begin
                Set (Create (Node_Sets.Element (From), To));
            end Add_Edge;
        begin
            Node_Sets.Iterate (After, Add_Edge'Access);
        end Make_Edges;

        procedure Make_Edges (To : in Node_Sets.Set) is
            procedure Add_From_Edge (From : in Node_Sets.Cursor) is
                procedure Add_To_Edge (Given : in Node_Sets.Cursor) is
                begin
                    Set (Create (
                        Node_Sets.Element (From),
                        Node_Sets.Element (Given)));
                end Add_To_Edge;
            begin
                Node_Sets.Iterate (To, Add_To_Edge'Access);
            end Add_From_Edge;
        begin
            Node_Sets.Iterate (After, Add_From_Edge'Access);
        end Make_Edges;
    begin
        if From = null then
            return;
        end if;

        case From.Get.Kind is
            when Process.Assignment_Event =>
                Append_Assignment (After, Into, From.Get);
            when Process.Call_Event =>
                Append_Call (After, Into, From.Get, Nodes);
            when Process.Condition_Event =>
                declare
                    Cond : Node := Create_Condition (Into, From.Get);
                    Next : Process.Processes.Node;
                    Tail : Node_Sets.Set;
                begin
                    Make_Edges (Cond);

                    Tail := Node_Sets.To_Set (Cond);
                    After := Node_Sets.Empty_Set;
                    From.First (Next);
                    while Next /= null loop
                        Generate (Next, Into, Nodes, Tail);
                        Next.Next (Next);
                        Node_Sets.Union (After, Tail);
                        Tail := Node_Sets.To_Set (Cond);
                    end loop;
                end;
            when Process.Branch_Event =>
                declare
                    Branches : Node_Sets.Set := Create_Branch (Into, From.Get);
                    Next : Process.Processes.Node;
                begin
                    Make_Edges (Branches);

                    From.First (Next);
                    while Next /= null loop
                        Generate (Next, Into, Nodes, Branches);
                        Next.Next (Next);
                    end loop;

                    After := Branches;
                end;
            when Process.Execution_Block =>
                declare
                    Cluster : Graph := New_Cluster (Into);
                    Execution : Node := Create_Execution (Cluster, From.Get);
                    Next : Process.Processes.Node;
                begin
                    Set_Style (Cluster, "rounded");
                    Make_Edges (Execution);

                    After := Node_Sets.To_Set (Execution);
                    From.First (Next);
                    while Next /= null loop
                        Generate (Next, Cluster, Nodes, After);
                        Next.Next (Next);
                    end loop;
                end;
            when Process.Normal_Block =>
                if From.Get.Name'Length > 0 then
                    declare
                        Cluster : Graph := New_Cluster (Into);
                        Start : Node := Create (Cluster);
                        Next : Process.Processes.Node;
                    begin
                        Set_Colour (Cluster, Name_Hash (From.Get.Name));
                        Set_Label (Start, From.Get.Name);
                        Set_Shape (Start, PLAINTEXT);

                        Node_Maps.Insert (Nodes, From.Get.Name, Start);

                        After := Node_Sets.To_Set (Start);
                        From.First (Next);
                        while Next /= null loop
                            Generate (Next, Cluster, Nodes, After);
                            Next.Next (Next);
                        end loop;
                    end;
                else
                    declare
                        Subgraph : Graph := New_Subgraph (Into);
                        Next : Process.Processes.Node;
                    begin
                        From.First (Next);
                        while Next /= null loop
                            Generate (Next, Subgraph, Nodes, After);
                            Next.Next (Next);
                        end loop;
                    end;
                end if;
        end case;
    end Generate;

    function Whole (From : Process.Processes.Node) return Graphviz.Graph is
        Result : Graph := New_Digraph;
        Hook : Node_Sets.Set := Node_Sets.Empty_Set;

        Cluster_Links : Node_Maps.Map := Node_Maps.Empty_Map;
    begin
        Generate (From, Result, Cluster_Links, Hook);

        return Result;
    end Whole;
end DOTProcess;
