with Tree_Buffers;
with Ada.Text_IO;
with Ada.Numerics.Float_Random; use Ada.Numerics.Float_Random;

procedure TBTest is
    package Char_Trees is new Tree_Buffers (Character);
    use Char_Trees;

    Root : Node := Create;

    task type A (C : Character);

    task body A is
        Top : Node := Create;
        Middle : Node := Create;
        Bottom_Left : Node := Create;
        Bottom_Right : Node := Create;
        Seed : Generator;
    begin
        Reset (Seed);

        Top.all.Set (C);
        Middle.all.Set (C);
        Bottom_Left.all.Set (C);
        Bottom_Right.all.Set (C);

        delay Duration (Random (Seed));

        Root.all.Prepend (Top);

        delay Duration (Random (Seed));

        Top.all.Append (Middle);

        delay Duration (Random (Seed));

        Middle.all.Prepend (Bottom_Left);

        delay Duration (Random (Seed));

        Middle.all.Append (Bottom_Right);

        delay Duration (Random (Seed));
    end A;

    X : A ('x');
    Y : A ('y');
    Z : A ('z');

    task type Printer is
        entry Print (Subtree : in Node);
    end Printer;

    task body Printer is
        Top, Middle, Bottom_Left, Bottom_Right : Node;
        Top_Name, Middle_Name, Bottom_Left_Name, Bottom_Right_Name : Character;
    begin
        accept Print (Subtree : in Node) do
            Top := Subtree;
        end Print;

        Top.all.Get (Top_Name);

        Ada.Text_IO.Put_Line ("Root -> " & Top_Name);

        Top.all.First_Child (Middle);
        Middle.all.Get (Middle_Name);

        Ada.Text_IO.Put_Line (Top_Name & " -> " & Middle_Name);

        Middle.all.First_Child (Bottom_Left);
        Bottom_Left.all.Get (Bottom_Left_Name);

        Ada.Text_IO.Put_Line (
            Top_Name & " -> " & Middle_Name & " -> " & Bottom_Left_Name);

        Bottom_Left.all.Next (Bottom_Right);
        Bottom_Right.all.Get (Bottom_Right_Name);

        Ada.Text_IO.Put_Line (
            Top_Name & " -> " & Middle_Name & " -> " & Bottom_Right_Name);
    end Printer;

    P1 : Printer;
    P2 : Printer;
    P3 : Printer;

    Subtree : Node;
begin
    Root.all.Last_Child (Subtree);
    P1.Print (Subtree);
    Subtree.all.Previous (Subtree);
    P2.Print (Subtree);
    Subtree.all.Previous (Subtree);
    P3.Print (Subtree);
end TBTest;
