with Process.Grammar;
with Process;
with DOTProcess;
with Graphviz;

with Ada.Text_IO;
with Ada.Exceptions;

procedure Main is
    Characters : aliased Process.Grammar.Character_Buffers.Buffer;
    Result : Process.Grammar.Grammar_Trees.Node :=
        Process.Grammar.Grammar_Trees.Create ((Kind => Process.Grammar.Program));
    Info : Process.Processes.Node := Process.Processes.Create ((
        Kind => Process.Normal_Block, Size => 0, Name => ""));
    Graph : Graphviz.Graph := Graphviz.New_Graph;

    task Parser;
    task body Parser is begin
        Process.Grammar.Parse (Characters'Access, Result);
    exception
        when E : Process.Grammar.Parse_Error =>
            Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (E));
    end Parser;

    task Builder;
    task body Builder is begin
        Process.Grammar.Parse (Result, Info);
    exception
        when E : Process.Grammar.Parse_Error =>
            Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (E));
    end Builder;

begin
    while not Ada.Text_IO.End_Of_File loop
        declare
            Line : String := Ada.Text_IO.Get_Line;
        begin
            for I in Line'Range loop
                Characters.Insert (Line (I));
            end loop;

            Characters.Insert (Character'Val (10));
        end;
    end loop;

    Characters.Insert (Character'First);

    Ada.Text_IO.Put_Line (Graphviz.Image (DOTProcess.Whole (Info)));
exception
    when E : others =>
        Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (E));
end Main;
