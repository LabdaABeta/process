with Ada.Unchecked_Deallocation;

package body Buffers is
    procedure Free_Node is new Ada.Unchecked_Deallocation (
        Element_Node, Element_Node_Access);
    protected body Buffer is
        entry Insert (Item : in Element) when Next_Node /= null is
        begin
            Next_Node.Value := Item;
            Next_Node.Next := null;
            if Head = null then
                Head := Next_Node;
                Tail := Next_Node;
            else
                Tail.Next := Next_Node;
                Tail := Next_Node;
            end if;

            Next_Node := new Element_Node;
        exception
            when Storage_Error =>
                Next_Node := null;
        end Insert;

        entry Remove (Item : out Element) when Head /= null is
            Removed : Element_Node_Access := Head;
        begin
            Item := Removed.Value;
            Head := Removed.Next;
            if Next_Node = null then
                Next_Node := Removed;
            else
                Free_Node (Removed);
            end if;
        end Remove;

        entry Replace (Item : in Element) when Next_Node /= null is
        begin
            Next_Node.Value := Item;
            Next_Node.Next := Head;
            if Head = null then
                Head := Next_Node;
                Tail := Next_Node;
            else
                Head := Next_Node;
            end if;

            Next_Node := new Element_Node;
        exception
            when Storage_Error =>
                Next_Node := null;
        end Replace;

        entry Peek (Item : out Element) when Head /= null is
        begin
            Item := Head.Value;
        end Peek;

        function As_Array return Element_Array is
            function Length (From : Element_Node_Access) return Natural is
            begin
                if From = null then
                    return 0;
                end if;
                return Length (From.Next) + 1;
            end Length;

            procedure Fill (
                Target : in out Element_Array;
                From : in Positive;
                Node : in Element_Node_Access) is
            begin
                if Node /= null then
                    Target (From) := Node.Value;
                    Fill (Target, From + 1, Node.Next);
                end if;
            end Fill;

            Result : Element_Array (1 .. Length (Head));
        begin
            Fill (Result, 1, Head);
            return Result;
        end As_Array;

    end Buffer;
end Buffers;
