with Concurrent_Trees;

package Process is
    type Variable_Reference (Size : Natural) is
        record
            Name : String (1 .. Size);
            Global : Boolean;
        end record;

    type Expression_Part_Type is (
        Variable_Value, Text_Value, Execution_Value, Root_Value);
    type Expression_Part (Kind : Expression_Part_Type; Size : Natural) is
        record
            case Kind is
                -- Leaves:
                when Variable_Value =>
                    Target : Variable_Reference (Size);
                when Text_Value =>
                    Text : String (1 .. Size);

                -- Internals:
                when Execution_Value | Root_Value =>
                    null;
            end case;
        end record;

    package Expressions is new Concurrent_Trees (Expression_Part);
    subtype Expression is Expressions.Node;

    -- An incident is a single event
    type Incident_Type is (Assignment_Event, Call_Event, Condition_Event,
        Branch_Event, Execution_Block, Normal_Block);
    type Incident (Kind : Incident_Type; Size : Natural) is
        record
            case Kind is
                -- Leaves:
                when Assignment_Event =>
                    Target : Variable_Reference (Size);
                    Value : Expression;
                when Call_Event =>
                    Callee : String (1 .. Size);

                -- Internals:
                when Condition_Event =>
                    Condition : Expression;
                when Branch_Event =>
                    -- Each child is one result
                    Result : Expression;
                when Execution_Block =>
                    Execution : Expression;
                when Normal_Block =>
                    Name : String (1 .. Size);
            end case;
        end record;

    package Processes is new Concurrent_Trees (Incident);
end Process;
