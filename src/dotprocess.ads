with Process;
with Graphviz;

package DOTProcess is
    Malformed_Error : exception;
    function Whole (From : Process.Processes.Node) return Graphviz.Graph;
end DOTProcess;

-- DOT Output:
--  Assignments    - An HTML table row with the variable name in the left column,
--                   an equals sign in the middle column, and the value in the
--                   right column.
--                   Local variables in the expression are italicized. Global
--                   Variables in the expression are italicized and underlined.
--                   Executions are in a sub-table.
--  Calls          - An HTML table row with the called block name spanning all
--                   columns (centered). An arrow comes from the call itself to the
--                   called cluster.
--  Conditions     - A diamond node showing the condition as an HTML table row.
--  Branches       - Lines from conditions to ellipses with the resulting value as
--                   an HTML table row.
--  Executions     - An unlabeled cluster starting with the execution in
--                   a parallelogram as an HTML table row.
--  Named Blocks   - An unlabeled cluster starting with the name in a plaintext
--                   node. The background colour is generated based on the name.
--                   The hue is determined from the name while the saturation is
--                   50% and the value is 100%. This ensures that black remains
--                   legible.
--  Unnamed Blocks - A subgraph containing the child nodes. Shows up only as
--                   layout clusters.
