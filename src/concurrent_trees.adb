with Ada.Unchecked_Deallocation;

package body Concurrent_Trees is
    protected body Node_Monitor is
        entry Append (Child : in Node) when not Capped is begin
            if Links.Tail = null then
                Links.Head := Child;
                Links.Tail := Child;
                Child.Set_Next (null);
            else
                Links.Tail.Set_Next (Child);
                Links.Tail := Child;
                Child.Set_Next (null);
            end if;
        end Append;

        procedure Cap is begin
            if Links.Tail /= null then
                Links.Tail.Set_Next (null, True);
            end if;
            Capped := True;
        end Cap;

        entry First (Child : out Node) when Links.Head /= null or Capped is
        begin
            Child := Links.Head;
        end First;

        entry Next (Sibling : out Node) when Links.Next /= null or Final is
        begin
            Sibling := Links.Next;
        end Next;

        function Get return Data is begin return Has.all; end Get;

        procedure Set_Next (To : in Node; Cap : in Boolean := False) is
        begin
            Links.Next := To;
            Final := Cap;
        end Set_Next;
    end Node_Monitor;

    function Create (On : Data) return Node is begin
        return new Node_Monitor (new Data'(On));
    end Create;

    function Leaf (On : Data) return Node is
        Result : Node := Create (On);
    begin
        Result.Cap;
        return Result;
    end Leaf;

    procedure Destroy (Item : in out Node) is
        Child : Node;
        Follow : Node;
        Target : Data_Access;

        procedure Free is new Ada.Unchecked_Deallocation (Node_Monitor, Node);
        procedure Free is new Ada.Unchecked_Deallocation (Data, Data_Access);
    begin
        if Item /= null then
            Item.First (Child);
            Item.Next (Follow);

            Destroy (Child);
            Destroy (Follow);

            Target := Item.Has;
            Free (Target);
            Free (Item);
        end if;
    end Destroy;
end Concurrent_Trees;
