with Ada.Exceptions;

package body Process.Grammar.Left is
    procedure Convert (
        From : access Left_Grammar_Trees.Node;
        Into : access Grammar_Trees.Node)
    is
    begin
        null; -- TODO
    end Convert;

    procedure Parse (
        From : access Character_Buffers.Buffer;
        Into : access Left_Grammar_Trees.Node)
    is
        Root : Left_Grammar_Trees.Node := Left_Grammar_Trees.Create (Accepted);
        Next : Character;

        procedure Fail (Msg : in String) is begin
            Ada.Exceptions.Raise_Exception (Parse_Error'Identity, Msg);
        end Fail;

        procedure Get_Next is begin
            From.Remove (Next);
        end Get_Next;

        function Ident_Char (Which : Character) return Boolean is begin
            case Which is
                when Character'First | ':' | '=' | ';' | '@' | '%' | '(' | ')' |
                    '?' | '{' | '}' | '!' | '[' | ']' | '$' | '"' | ' ' =>
                    return False;
                when others =>
                    return Ada.Characters.Handling.Is_Graphic (Which);
            end case;
        end Ident_Char;

        function Accept_On (Expected : Character) return Boolean is begin
            if Next = Expected then
                Get_Next;
                return True;
            end if;

            return False;
        end Accept_On;

        procedure Expect (
            Expected : in Character;
            Into : in out Left_Grammar_Trees.Node) is begin
            if Accept_On (Expected) then
                Set_Lexeme (Into, Expected);
            else
                if Expected = Character''First then
                    Error ("Expected end of file but found " & Next);
                elsif Next = Character'First then
                    Fail ("Expected " & Expected & " but found EOF");
                else
                    Error ("Expected " & Expected & " but found " & Next);
                end if;
            end if;
        end Expect;

        procedure Parse_Accepted (Into : in out Left_Grammar_Trees.Node);

    begin
        Get_Next;
        Output.all := Root;
        Parse_Accepted (Root);
    end Parse;
end Process.Grammar.Left;
