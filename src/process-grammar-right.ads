with Tree_Buffers;

package Process.Grammar.Right is
    type Symbol is (
        Accepted, Program, Event, Variable, Expression, Branches,
        Expression_Part, Branch, Identifier, Text, Terminal, EOF);

    type Rule is (
        Accepted,            -- $accept         -> Process $end
        No_More_Events,      -- Process         ->
        More_Events,         -- Process         -> Process Event
        Assignment_Event,    -- Event           -> Variable : = Expression ;
        Call_Event,          -- Event           -> @ Identifier ;
        Execute_Event,       -- Event           -> % ( Expression ) { Process }
        Branch_Event,        -- Event           -> ? ( Expression ) { Branches }
        Named_Block_Event,   -- Event           -> Identifier { Process }
        Block_Event,         -- Event           -> { Process }
        Global_Variable,     -- Variable        -> ( Identifier )
        Local_Variable,      -- Variable        -> [ Identifier ]
        Expression,          -- Expression      -> Expression_Part
        More_Expression,     -- Expression      -> Expression Expression_Part
        No_More_Branches,    -- Branches        ->
        More_Branches,       -- Branches        -> Branches Branch
        Variable_Expression, -- Expression_Part -> Variable
        Execute_Expression,  -- Expression_Part -> % ( Expression )
        Text_Expression,     -- Expression_Part -> q Text q
        Branch,              -- Branch          -> Expression : Event
        Identifier,          -- Identifier      -> a
        More_Identifier,     -- Identifier      -> Identifier a
        No_More_Text,        -- Text            ->
        More_Text,           -- Text            -> Text s
        Terminal); -- No need for EOF - Each rule has fixed size!

    package Right_Grammar_Trees is new Tree_Buffers (Rule, Token);

    function Convert (From : Right_Grammar_Trees.Node)
        return Grammar_Trees.Node;
end Process.Grammar.Right;
