with Ada.Containers.Hashed_Sets;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Hash;
with Ada.Characters.Handling;

package body Graphviz is
    package Graph_Sets is new Ada.Containers.Hashed_Sets (Graph, Hash, "=");
    package Node_Sets is new Ada.Containers.Hashed_Sets (Node, Hash, "=");
    package Edge_Sets is new Ada.Containers.Hashed_Sets (Edge, Hash, "=");
    use Graph_Sets;
    use Node_Sets;
    use Edge_Sets;

    type Graph_Kind is (Directed, Undirected, Subgraph, Cluster);

    type Element_Id is mod 2 ** 64;

    function Image (Item : in Element_Id) return String is
        Number : String := Element_Id'Image (Item);
    begin
        return "i" & Number (Number'First + 1 .. Number'Last);
    end Image;

    type Graph_Data is
        record
            Parent : Graph;
            Id : Element_Id;
            Col : Colour;
            Label : Unbounded_String;
            Kind : Graph_Kind;
            Children : Graph_Sets.Set;
            Contents : Node_Sets.Set;
            Edges : Edge_Sets.Set;
            Style : Unbounded_String;
        end record;

    type Node_Data is
        record
            Parent : Graph;
            Id : Element_Id;
            Col : Colour;
            Label : Unbounded_String;
            Shape : Shape_Kind;
            Label_Type : Label_Kind;
            Orientation : Integer;
        end record;

    type Edge_Data is
        record
            Source : Node;
            Target : Node;
            From_Port : Unbounded_String;
            To_Port : Unbounded_String;
            Id : Element_Id;
            Col : Colour;
            Label : Unbounded_String;
            Dir : Direction_Type;
        end record;

    Last_Id : Element_Id := 0;
    function Next_Id return Element_Id is begin
        Last_Id := Last_Id + 1;
        return Last_Id;
    end Next_Id;

    function Open (Item : Label_Kind) return String is
    begin
        if Item = TEXT_BASED then
            return """";
        else
            return "<";
        end if;
    end Open;

    function Close (Item : Label_Kind) return String is
    begin
        if Item = TEXT_BASED then
            return """";
        else
            return ">";
        end if;
    end Close;

    function Image (Item : Colour) return String is
        subtype String_Pair is String (1 .. 2);
        Hex : constant array (RGB_Component) of String_Pair := (
            "00", "01", "02", "03", "04", "05", "06", "07",
            "08", "09", "0a", "0b", "0c", "0d", "0e", "0f",
            "10", "11", "12", "13", "14", "15", "16", "17",
            "18", "19", "1a", "1b", "1c", "1d", "1e", "1f",
            "20", "21", "22", "23", "24", "25", "26", "27",
            "28", "29", "2a", "2b", "2c", "2d", "2e", "2f",
            "30", "31", "32", "33", "34", "35", "36", "37",
            "38", "39", "3a", "3b", "3c", "3d", "3e", "3f",
            "40", "41", "42", "43", "44", "45", "46", "47",
            "48", "49", "4a", "4b", "4c", "4d", "4e", "4f",
            "50", "51", "52", "53", "54", "55", "56", "57",
            "58", "59", "5a", "5b", "5c", "5d", "5e", "5f",
            "60", "61", "62", "63", "64", "65", "66", "67",
            "68", "69", "6a", "6b", "6c", "6d", "6e", "6f",
            "70", "71", "72", "73", "74", "75", "76", "77",
            "78", "79", "7a", "7b", "7c", "7d", "7e", "7f",
            "80", "81", "82", "83", "84", "85", "86", "87",
            "88", "89", "8a", "8b", "8c", "8d", "8e", "8f",
            "90", "91", "92", "93", "94", "95", "96", "97",
            "98", "99", "9a", "9b", "9c", "9d", "9e", "9f",
            "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7",
            "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af",
            "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7",
            "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf",
            "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7",
            "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf",
            "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7",
            "d8", "d9", "da", "db", "dc", "dd", "de", "df",
            "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7",
            "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef",
            "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7",
            "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff");
    begin
        if Item = Colourless then
            return "transparent";
        end if;

        if Item.Kind = RGB then
            return "#" &
                Hex (Item.R) &
                Hex (Item.G) &
                Hex (Item.B) &
                Hex (Item.A);
        else
            return HSV_Component'Image (Item.H) &
                HSV_Component'Image (Item.S) &
                HSV_Component'Image (Item.V);
        end if;
    end Image;

    function Hash (Item : Graph) return Ada.Containers.Hash_Type is begin
        return Ada.Strings.Hash (Image (Item.Id));
    end Hash;

    function New_Graph return Graph is begin
        return new Graph_Data'(
            Parent => null,
            Id => Next_Id,
            Col => Colourless,
            Label => Null_Unbounded_String,
            Kind => Undirected,
            Children => Graph_Sets.Empty_Set,
            Contents => Node_Sets.Empty_Set,
            Edges => Edge_Sets.Empty_Set,
            Style => To_Unbounded_String ("filled"));
    end New_Graph;

    function New_Digraph return Graph is begin
        return new Graph_Data'(
            Parent => null,
            Id => Next_Id,
            Col => Colourless,
            Label => Null_Unbounded_String,
            Kind => Directed,
            Children => Graph_Sets.Empty_Set,
            Contents => Node_Sets.Empty_Set,
            Edges => Edge_Sets.Empty_Set,
            Style => To_Unbounded_String ("filled"));
    end New_Digraph;

    function New_Subgraph (Under : Graph) return Graph is
        Result : Graph := new Graph_Data'(
            Parent => Under,
            Id => Next_Id,
            Col => Colourless,
            Label => Null_Unbounded_String,
            Kind => Subgraph,
            Children => Graph_Sets.Empty_Set,
            Contents => Node_Sets.Empty_Set,
            Edges => Edge_Sets.Empty_Set,
            Style => To_Unbounded_String ("filled"));
    begin
        Graph_Sets.Insert (Under.Children, Result);
        return Result;
    end New_Subgraph;

    function New_Cluster (Under : Graph) return Graph is
        Result : Graph := new Graph_Data'(
            Parent => Under,
            Id => Next_Id,
            Col => Colourless,
            Label => Null_Unbounded_String,
            Kind => Cluster,
            Children => Graph_Sets.Empty_Set,
            Contents => Node_Sets.Empty_Set,
            Edges => Edge_Sets.Empty_Set,
            Style => To_Unbounded_String ("filled"));
    begin
        Graph_Sets.Insert (Under.Children, Result);
        return Result;
    end New_Cluster;

    procedure Set_Colour (Item : in out Graph; To : in Colour) is
    begin
        Item.Col := To;
    end Set_Colour;

    procedure Set_Label (Item : in out Graph; To : in String) is
    begin
        Item.Label := To_Unbounded_String (To);
    end Set_Label;

    procedure Set_Style (Item : in out Graph; To : in String) is
    begin
        Item.Style := To_Unbounded_String (To);
    end Set_Style;

    function Image (Item : Graph) return String is
        function Open return String is begin
            case Item.Kind is
                when Directed =>
                    return "digraph " & Image (Item.Id) & " {";
                when Undirected =>
                    return "graph " & Image (Item.Id) & " {";
                when Subgraph =>
                    return "subgraph {";
                when Cluster =>
                    return "subgraph cluster_" & Image (Item.Id) & " {";
            end case;
        end Open;

        function Properties return String is
            function Fill_Color return String is begin
                if Item.Col /= Colourless then
                    return "fillcolor=""" & Image (Item.Col) & """;";
                else
                    return "";
                end if;
            end Fill_Color;

            function Label return String is begin
                if Length (Item.Label) > 0 then
                    return "label=""" & To_String (Item.Label) & """;";
                else
                    return "";
                end if;
            end Label;
        begin
            if Item.Kind = Subgraph or Item.Kind = Cluster then
                return To_String ("style=" & Item.Style & ";" &
                    Fill_Color & Label);
            else
                return
                    "nodesep=0;" &
                    "ranksep=0.02;" &
                    "compound=true;" &
                    "concentrate=true;" &
                    "pack=true;" &
                    "remincross=true;" &
                    "packMode=clust;";
            end if;
        end Properties;

        function Children return String is
            Result : Unbounded_String;

            procedure Append (Position : in Graph_Sets.Cursor) is begin
                Result := Result & Image (Graph_Sets.Element (Position));
            end Append;
        begin
            Iterate (Item.Children, Append'Access);

            return To_String (Result);
        end Children;

        function Nodes return String is
            Result : Unbounded_String;

            procedure Append (Position : in Node_Sets.Cursor) is begin
                Result := Result & Image (Node_Sets.Element (Position));
            end Append;
        begin
            Iterate (Item.Contents, Append'Access);

            return To_String (Result);
        end Nodes;

        function Edges_Of (From : Graph) return String is
            Result : Unbounded_String;

            procedure Append (Position : in Edge_Sets.Cursor) is begin
                Result := Result & Image (Edge_Sets.Element (Position));
            end Append;
        begin
            Iterate (From.Edges, Append'Access);

            return To_String (Result);
        end Edges_Of;

        function Edges (From : Graph) return String is
            Result : Unbounded_String;

            procedure Append (Position : in Graph_Sets.Cursor) is begin
                Result := Result & Edges (Graph_Sets.Element (Position));
            end Append;
        begin
            Result := To_Unbounded_String (Edges_Of (From));
            Iterate (From.Children, Append'Access);
            return To_String (Result);
        end Edges;

        function All_Edges return String is
        begin
            if Item.Kind = Subgraph or Item.Kind = Cluster then
                return "";
            else
                return Edges (Item);
            end if;
        end All_Edges;
    begin
        return Open & Properties & Nodes & Children & All_Edges & "}";
    end Image;

    function Hash (Item : Node) return Ada.Containers.Hash_Type is begin
        return Ada.Strings.Hash (Image (Item.Id));
    end Hash;

    function Create (Under : Graph) return Node is
        Result : Node := new Node_Data'(
            Parent => Under,
            Id => Next_Id,
            Col => (Kind => RGB, R => 0, G => 0, B => 0, A => 255),
            Label => Null_Unbounded_String,
            Shape => ELLIPSE,
            Label_Type => TEXT_BASED,
            Orientation => 0);
    begin
        Under.Contents.Insert (Result);
        return Result;
    end Create;

    procedure Set_Colour (Item : in out Node; To : in Colour) is
    begin
        Item.Col := To;
    end Set_Colour;

    procedure Set_Label (Item : in out Node; To : in String) is
    begin
        Item.Label := To_Unbounded_String (To);
    end Set_Label;

    procedure Set_Shape (Item : in out Node; To : in Shape_Kind) is
    begin
        Item.Shape := To;
    end Set_Shape;

    procedure Set_Label_Kind (Item : in out Node; To : in Label_Kind) is
    begin
        Item.Label_Type := To;
    end Set_Label_Kind;

    procedure Set_Orientation (Item : in out Node; To : in Integer) is
    begin
        Item.Orientation := To;
    end Set_Orientation;

    function Get_Label (Item : Node) return String is begin
        return To_String (Item.Label);
    end Get_Label;

    function Get_Shape (Item : Node) return Shape_Kind is begin
        return Item.Shape;
    end Get_Shape;

    function Get_Parent (Item : Node) return Graph is begin
        return Item.Parent;
    end Get_Parent;

    function Get_Id (Item : Node) return String is
    begin
        return Image (Item.Id);
    end Get_Id;

    function Image (Item : Node) return String is
        function Label return String is begin
            if Length (Item.Label) > 0 then
                return "label=" & Open (Item.Label_Type) &
                    To_String (Item.Label) & Close (Item.Label_Type);
            else
                return "";
            end if;
        end Label;

        function Color return String is begin
            if Item.Col /= (Kind => RGB, R => 0, G => 0, B => 0, A => 255) then
                return " color=""" & Image (Item.Col) & """";
            else
                return "";
            end if;
        end Color;

        function Shape return String is begin
            if Item.Shape /= ELLIPSE then
                return " shape=" & Ada.Characters.Handling.To_Lower (
                    Shape_Kind'Image (Item.Shape));
            else
                return "";
            end if;
        end Shape;

        function Orientation return String is begin
            if Item.Orientation /= 0 then
                return " orientation=" & Integer'Image (Item.Orientation);
            else
                return "";
            end if;
        end Orientation;
    begin
        return Image (Item.Id) & "[" & Label & Color & Shape & Orientation &
            "];";
    end Image;

    function Hash (Item : Edge) return Ada.Containers.Hash_Type is begin
        return Ada.Strings.Hash (Image (Item.Id));
    end Hash;

    function Create (From, To : Node) return Edge is
        Result : Edge := new Edge_Data'(
            Source => From,
            Target => To,
            From_Port => Null_Unbounded_String,
            To_Port => Null_Unbounded_String,
            Id => Next_Id,
            Col => (Kind => RGB, R => 0, G => 0, B => 0, A => 255),
            Label => Null_Unbounded_String,
            Dir => FORWARD);
    begin
        if From = null or To = null then
            return null;
        end if;

        From.Parent.Edges.Insert (Result);
        return Result;
    end Create;

    procedure Set_Colour (Item : in out Edge; To : in Colour) is
    begin
        Item.Col := To;
    end Set_Colour;

    procedure Set_Label (Item : in out Edge; To : in String) is
    begin
        Item.Label := To_Unbounded_String (To);
    end Set_Label;

    procedure Set_Dir (Item : in out Edge; To : in Direction_Type := FORWARD) is
    begin
        Item.Dir := To;
    end Set_Dir;

    procedure Set_From_Port (Item : in out Edge; To : in String) is
    begin
        Item.From_Port := To_Unbounded_String (To);
    end Set_From_Port;

    procedure Set_To_Port (Item : in out Edge; To : in String) is
    begin
        Item.To_Port := To_Unbounded_String (To);
    end Set_To_Port;

    procedure Set (Item : in Edge) is begin null; end Set;

    function Image (Item : Edge) return String is
        function Port_Of (Port : in Unbounded_String) return String is
        begin
            if Port = Null_Unbounded_String then
                return "";
            else
                return To_String (":" & Port);
            end if;
        end Port_Of;

        function Arrow return String is
            Root_Graph : Graph := Item.Source.Parent;
        begin
            while Root_Graph.Kind = Subgraph or Root_Graph.Kind = Cluster loop
                Root_Graph := Root_Graph.Parent;
            end loop;

            if Root_Graph.Kind = Undirected then
                return "--";
            else
                return "->";
            end if;
        end Arrow;

        function Label return String is begin
            -- Always show the label, as the default will be i####
            return "[label=""" & To_String (Item.Label) & """]";
        end Label;

        function Color return String is begin
            if Item.Col /= (Kind => RGB, R => 0, G => 0, B => 0, A => 255) then
                return "[color=""" & Image (Item.Col) & """]";
            else
                return "";
            end if;
        end Color;

        function Direction return String is
            Root_Graph : Graph := Item.Source.Parent;
        begin
            while Root_Graph.Kind = Subgraph or Root_Graph.Kind = Cluster loop
                Root_Graph := Root_Graph.Parent;
            end loop;

            if (Root_Graph.Kind = Undirected and Item.Dir /= NONE) or
               (Root_Graph.Kind = Directed and Item.Dir /= FORWARD)
            then
               return "[dir=" & Ada.Characters.Handling.To_Lower (
                   Direction_Type'Image (Item.Dir)) & "]";
            else
                return "";
            end if;
        end Direction;
    begin
        return Image (Item.Source.Id) & Port_Of (Item.From_Port) &
            Arrow & Image (Item.Target.Id) & Port_Of (Item.To_Port) &
            Label & Color & Direction & ";";
    end Image;

end Graphviz;
