with Buffers;
with Concurrent_Trees;

package Process.Grammar is
    type Symbol is (
        Program,      -- Event* EOF
        Event,        -- Single_Event | Block_Event
        Single_Event, -- (Assignment | Call) ';'
        Block_Event,  -- Condition | Group
        Assignment,   -- Variable ':' '=' Expression
        Call,         -- '@' identifier
        Condition,    -- '?' '(' Expression ')' '{' Branch* '}'
        Group,        -- (Execution | identifier)? '{' Program '}'
        Variable,     -- '(' identifier ')' | '[' identifier ']'
        Expression,   -- (Variable | Text | Execution)* EOF
        Execution,    -- '%' '(' Expression ')'
        Branch,       -- Results Event
        Results,      -- ':' | Expression ':' Results
        Text,         -- '"' escaped '"'
        Identifier,   -- Things delimited
        Terminal);

    Parse_Error : exception;

    type Production (Kind : Symbol) is
        record
            case Kind is
                when Terminal =>
                    Data : Character;
                when others =>
                    null;
            end case;
        end record;

    package Grammar_Trees is new Concurrent_Trees (Production);
    package Character_Buffers is new Buffers (Character);

    procedure Parse (
        From : access Character_Buffers.Buffer;
        Into : in out Grammar_Trees.Node);

    procedure Parse (
        From : in Grammar_Trees.Node;
        Into : in out Processes.Node);
end Process.Grammar;
