with Tree_Buffers;

package Process.Grammar.Left is
    type Symbol is (
        Accepted, Program, Event, Variable, Expression, More_Expression,
        Branches, Expression_Part, Branch, Identifier, More_Identifier,
        Text, Terminal, EOF);

    type Rule is (
        Accepted,            -- $accept         -> Program $end
        No_More_Events,      -- Program         ->
        More_Events,         -- Program         -> Event Program
        Assignment_Event,    -- Event           -> Variable : = Expression ;
        Call_Event,          -- Event           -> @ Identifier ;
        Execute_Event,       -- Event           -> % ( Expression ) { Process }
        Branch_Event,        -- Event           -> ? ( Expression ) { Branches }
        Named_Block_Event,   -- Event           -> Identifier { Process }
        Block_Event,         -- Event           -> { Process }
        Global_Variable,     -- Variable        -> ( Identifier )
        Local_Variable,      -- Variable        -> [ Identifier ]
        Expression,          -- Expression      -> Expression_Part More_Expression
        No_More_Expression,  -- More_Expression ->
        More_Expression,     -- More_Expression -> Expression_Part Expression
        No_More_Branches,    -- Branches        ->
        More_Branches,       -- Branches        -> Branch Branches
        Variable_Expression, -- Expression_Part -> Variable
        Execute_Expression,  -- Expression_Part -> % ( Expression )
        Text_Expression,     -- Expression_Part -> q Text q
        Branch,              -- Branch          -> Expression : Event
        Identifier,          -- Identifier      -> a More_Identifier
        No_More_Identifier,  -- More_Identifier ->
        More_Identifier,     -- More_Identifier -> a More_Identifier
        No_More_Text,        -- Text            ->
        More_Text,           -- Text            -> s Text
        Terminal); -- No need for EOF - Each rule has fixed size!

    package Left_Grammar_Trees is new Tree_Buffers (Rule, Character);

    procedure Convert (
        From : access Left_Grammar_Trees.Node;
        Into : access Grammar_Trees.Node);

    procedure Parse (
        From : access Character_Buffers.Buffer;
        Into : access Left_Grammar_Trees.Node);
end Process.Grammar.Left;
