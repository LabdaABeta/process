generic
    type Data (<>) is private; -- The data associated with a node
package Concurrent_Trees is
    type Data_Access is access all Data;

    type Node_Monitor;
    type Node is access all Node_Monitor;

    type Link_Set is
        record
            Head : Node := null;
            Tail : Node := null;
            Next : Node := null;
        end record;

    protected type Node_Monitor (Has : Data_Access) is
        entry Append (Child : in Node);
        procedure Cap; -- no more appends

        entry First (Child : out Node);
        entry Next (Sibling : out Node);

        function Get return Data;

        -- Internal use
        procedure Set_Next (To : in Node; Cap : in Boolean := False);
    private
        Links : Link_Set;
        Final : Boolean := False;
        Capped : Boolean := False;
    end Node_Monitor;

    function Create (On : Data) return Node;
    function Leaf (On : Data) return Node;
    procedure Destroy (Item : in out Node); -- Recursive
end Concurrent_Trees;

