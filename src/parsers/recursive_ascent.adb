-- BIG TODO: Check all uses of Ident_Char and make them work correctly with
-- escapes (or remove escapes from the grammar entirely???)
with Ada.Characters.Handling;
with Process.Parsing; use Process.Parsing;

-- Generated based on:
--
--    %%
--    Process         : | Event Process;
--    Event           : Variable ':' '=' Expression ';'
--                    | '@' Identifier ';'
--                    | '%' '(' Expression ')' ';'
--                    | '?' '(' Expression ')' '{' Cases '}'
--                    | Identifier '{' Process '}'
--                    | '{' Process '}'
--                    | Table_Header Table_Rows;
--    Table_Header    : '$' Variable ':' ':' Columns 'x';
--    Table_Rows      : | Table_Row Table_Rows;
--    Variable        : '(' Identifier ')'
--                    | '[' Identifier ']';
--    Expression      : | Expression_Part Expression;
--    Cases           : | Case Cases;
--    Columns         : Expression ':' More_Columns;
--    More_Columns    : | Expression ':' More_Columns;
--    Table_Row       : 'x' ':' Expression ':' ':' Columns 'x';
--    Expression_Part : Variable
--                    | '%' '(' Expression ')'
--                    | 'q' String 'q';
--    Case            : Expression ':' Event;
--    Identifier      : 'a' More_Text;
--    More_Text       : | 'a' More_Text;
--    String          : | 's' String;
--
--  At: zaa.ch/jison/try/usf
--  As: SLR(1)

separate (Parsers)
procedure Do_Parse (
    Input : access Character_Buffers.Buffer;
    Output : access Parse_Forests.Buffer) is
    -- Uses output as a stack, thus Replace() instead of Insert()

    Next : Character;

    procedure Get_Next is begin
        Input.Remove (Next);
    end Get_Next;

    function Ident_Char (Which : Character) return Boolean is
    begin
        case Which is
            when Character'First | ':' | '=' | ';' | '@' | '%' | '(' | ')' |
                '?' | '{' | '}' | '!' | '[' | ']' | '$' | '"' | ' ' =>
                return False;
            when others =>
                return Ada.Characters.Handling.Is_Graphic (Which);
        end case;
    end Ident_Char;

    function Reduce (Size : Child_Count; Kind : Symbol) return Natural is
        Top : Parse_Node;
        Created : Parse_Node := Create (Kind);
    begin
        for I in Child_Index range 1 .. Size loop
            Output.Remove (Children (I));
            Insert (Created, Top);
        end loop;

        Output.Replace (Created);

        return Natural (Size);
    end Reduce;

    -- States:
    function Parse_Beginning                     return Natural; -- 0
    function Finished                            return Natural; -- 1
    function More_Processes                      return Natural; -- 2
    function Assignment_Operator                 return Natural; -- 3
    function Call_Identifier                     return Natural; -- 4
    function Execution_Event                     return Natural; -- 5
    function Condition_Identifier                return Natural; -- 6
    function Named_Block_Start                   return Natural; -- 7
    function Unnamed_Block                       return Natural; -- 8
    function First_Table_Row                     return Natural; -- 9
    function Global_Identifier                   return Natural; -- 10
    function Local_Identifier                    return Natural; -- 11
    function More_Identifier                     return Natural; -- 12
    function Table_Header_Variable               return Natural; -- 13
    function Done_More_Process                   return Natural; -- 14
    function Assignment_Equals                   return Natural; -- 15
    function End_Call_Identifier                 return Natural; -- 16
    function Execution_Event_Expression          return Natural; -- 17
    function Condition_Identifier_Expression     return Natural; -- 18
    function Named_Block_Process                 return Natural; -- 19
    function End_Block                           return Natural; -- 20
    function No_More_Table_Rows                  return Natural; -- 21
    function More_Table_Rows                     return Natural; -- 22
    function Start_Table_Row                     return Natural; -- 23
    function End_Global_Identifier               return Natural; -- 24
    function End_Local_Identifier                return Natural; -- 25
    function End_Identifier                      return Natural; -- 26
    function Even_More_Text                      return Natural; -- 27
    function Table_Header_Column_Sep             return Natural; -- 28
    function Assignment_Expression               return Natural; -- 29
    function End_Call                            return Natural; -- 30
    function Close_Execution_Event               return Natural; -- 31
    function More_Expression                     return Natural; -- 32
    function Variable_Expression_Part            return Natural; -- 33
    function Execution_Expression_Part_Start     return Natural; -- 34
    function Text_Expression_Part_Start          return Natural; -- 35
    function End_Condition_Identifier_Expression return Natural; -- 36
    function End_Named_Process                   return Natural; -- 37
    function End_Unnamed_Process                 return Natural; -- 38
    function End_Table                           return Natural; -- 39
    function Table_Row_Value_Expression          return Natural; -- 40
    function End_Global_Variable                 return Natural; -- 41
    function End_Local_Variable                  return Natural; -- 42
    function No_More_Text                        return Natural; -- 43
    function Table_Header_Column_Start           return Natural; -- 44
    function End_Assignment_Expression           return Natural; -- 45
    function End_Execution_Event_Expression      return Natural; -- 46
    function No_More_Expression                  return Natural; -- 47
    function Start_Expression                    return Natural; -- 48
    function End_Text                            return Natural; -- 49
    function More_String                         return Natural; -- 50
    function Start_Cases                         return Natural; -- 51
    function End_Named_Block                     return Natural; -- 52
    function Table_Row_Column_Sep                return Natural; -- 53
    function Table_Row_Column                    return Natural; -- 54
    function End_Assignment                      return Natural; -- 55
    function End_Execution_Event                 return Natural; -- 56
    function End_Execution_Expression            return Natural; -- 57
    function End_Text_Expression                 return Natural; -- 58
    function End_String                          return Natural; -- 59
    function Start_Case                          return Natural; -- 60
    function Table_Row_Column_Start              return Natural; -- 61
    function Table_Row_End                       return Natural; -- 62
    function Start_More_Columns                  return Natural; -- 63
    function End_Execution_Expression_Part       return Natural; -- 64
    function End_Cases                           return Natural; -- 65
    function More_Cases                          return Natural; -- 66
    function Case_Separator                      return Natural; -- 67
    function Start_Columns                       return Natural; -- 68
    function End_Table_Header                    return Natural; -- 69
    function Even_More_Columns                   return Natural; -- 70
    function End_Condition_Event                 return Natural; -- 71
    function No_More_Cases                       return Natural; -- 72
    function Case_Event                          return Natural; -- 73
    function Table_Row_Final                     return Natural; -- 74
    function No_More_Columns                     return Natural; -- 75
    function Even_More_Columns_Sep               return Natural; -- 76
    function End_Case                            return Natural; -- 77
    function End_Table_Row                       return Natural; -- 78
    function Start_Even_More_Columns             return Natural; -- 79
    function End_Even_More_Columns               return Natural; -- 80

    -- Parse Helpers
    procedure Shift is
        Result : Parse_Node;
    begin
        Result := Create (Terminal);
        Set_Lexeme (Result, Next);
        Output.Replace (Result);
        Input.Remove (Next);
    end Shift;

    function Parse_Process (
        Followup : access function return Natural)
        return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            case Kind (Top) is
                when Event => return Gotos (More_Processes);
                when Identifier => return Gotos (Named_Block_Start);
                when Process => return Gotos (Followup.all);
                when Table_Header => return Gotos (First_Table_Row);
                when Variable => return Gotos (Assignment_Operator);
                when others =>
                    Error ("Couldn't reduce past " &
                        Symbol'Image (Kind (Top)) & " in a process.");
                    return 0;
            end case;
        end Gotos;
    begin
        case Next is
            when '$' => Shift; return Gotos (Table_Header_Variable);
            when Character'First => return Gotos (Reduce (0, Processed));
            when '%' => Shift; return Gotos (Execution_Event);
            when '(' => Shift; return Gotos (Global_Identifier);
            when '?' => Shift; return Gotos (Condition_Identifier);
            when '@' => Shift; return Gotos (Call_Identifier);
            when '[' => Shift; return Gotos (Local_Identifier);
            when '{' => Shift; return Gotos (Unnamed_Block);
            when '}' => return Gotos (Reduce (0, Processed));
            when others =>
                if Ident_Char (Next) then
                    Shift; return Gotos (More_Identifier);
                else
                    Error ("Didn't expect '" & Next & "' in a process.");
                    return 0;
                end if;
        end case;
    end Parse_Process;

    function Parse_Expression (
        Followup : access function return Natural;
        Columnup : access function return Natural := null;
        Caseup : access function return Natural := null;
        Casesup : access function return Natural := null)
        return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            case Kind (Top) is
                when Columns =>
                    if Columnup /= null then
                        return Gotos (Columnup.all);
                    else
                        Error ("Couldn't reduce past " &
                            Symbol'Image (Kind (Top)) & " in an expression.");
                    end if;
                when Expression => return Gotos (Followup.all);
                when Expression_Part => return Gotos (More_Expression);
                when Variable => return Gotos (Variable_Expression_Part)
                when Cases =>
                    if Casesup /= null then
                        return Gotos (Casesup.all);
                    else
                        Error ("Couldn't reduce past " &
                            Symbol'Image (Kind (Top)) & " in an expression.");
                    end if;
                when Case =>
                    if Caseup /= null then
                        return Gotos (Caseup.all);
                    else
                        Error ("Couldn't reduce past " &
                            Symbol'Image (Kind (Top)) & " in an expression.");
                    end if;
                when others =>
                    Error ("Couldn't reduce past " &
                        Symbol'Image (Kind (Top)) & " in an expression.");
            end case;
        end Gotos;
    begin
        case Next is
            when '%' => Shift; return Gotos (Execution_Expression_Part_Start);
            when '(' => Shift; return Gotos (Global_Identifier);
            when ')' | ':' | ';' => return Gotos (Reduce (0, Expression));
            when '[' => Shift; return Gotos (Local_Identifier);
            when '"' => Shift; return Gotos (Text_Expression_Part_Start);
            when '}' =>
                if Caseup /= null or Casesup /= null then
                    return Gotos (Reduce (0, Cases));
                else
                    Error ("Didn't expect " & Next & " in an expression.");
                end if;
            when others =>
                Error ("Didn't expect " & Next & " in an expression.");
                return 0;
        end case;
    end Parse_Expression;

    -- $accept -> .Process $end #lookaheads= $end
    -- Process -> .
    -- Process -> .Event Process
    -- Event -> .Variable : = Expression ;
    -- Event -> .@ Identifier ;
    -- Event -> .% ( Expression ) ;
    -- Event -> .? ( Expression ) { Cases }
    -- Event -> .Identifier { Process }
    -- Event -> .{ Process }
    -- Event -> .Table_Header Table_Rows
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    -- Identifier -> .a More_Text
    -- Table_Header -> .$ Variable : : Columns x
    function Parse_Beginning return Natural is begin
        return Parse_Process (Finished'Access);
    end Parse_Beginning;

    -- $accept -> Process .$end #lookaheads= $end
    function Finished return Natural is
        Nodes : array (Child_Index) of Parse_Node;
    begin
        if Next = Character'First then
            return Reduce (1, Accepted) - 1;
        else
            Error ("Didn't expect '" & Next & "' at the end.");
            return 0;
        end if;
    end Finished;

    -- Process -> Event .Process
    -- Process -> .
    -- Process -> .Event Process
    -- Event -> .Variable : = Expression ;
    -- Event -> .@ Identifier ;
    -- Event -> .% ( Expression ) ;
    -- Event -> .? ( Expression ) { Cases }
    -- Event -> .Identifier { Process }
    -- Event -> .{ Process }
    -- Event -> .Table_Header Table_Rows
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    -- Identifier -> .a More_Text
    -- Table_Header -> .$ Variable : : Columns x
    function More_Processes return Natural is
    begin
        return Parse_Process (Done_More_Process'Access);
    end More_Processes;

    -- Event -> Variable .: = Expression ;
    function Assignment_Operator return Natural is
    begin
        if Next = ':' then
            Shift; return Assignment_Equals - 1;
        else
            Error ("Expected : in assignment operator.");
            return 0;
        end if;
    end Assignment_Operator;

    -- Event -> @ .Identifier ;
    -- Identifier -> .a More_Text
    function Call_Identifier return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
               return Depth - 1;
            end if;

            Output.Peek (Top);

            if Kind (Top) = Identifier then
                return Gotos (End_Call_Identifier);
            else
                Error ("Couldn't reduce past " & Symbol'Image (Kind (Top)) &
                    " in call identifier.");
            end if;
        end Gotos;
    begin
        if Ident_Char (Next) then
            Shift; return Gotos (More_Identifier);
        else
            Error ("Expected an identifier in the call.");
            return 0;
        end if;
    end Call_Identifier;

    -- Event -> % .( Expression ) ;
    function Execution_Event return Natural is
    begin
        if Next = '(' then
            Shift; return Execution_Event_Expression - 1;
        else
            Error ("Expected ( in execution event.");
            return 0;
        end if;
    end Execution_Event;

    -- Event -> ? .( Expression ) { Cases }
    function Condition_Identifier return Natural is
    begin
        if Next = '(' then
            Shift; return Condition_Identifier_Expression - 1;
        else
            Error ("Expected ( in condition identifier.");
            return 0;
        end if;
    end Condition_Identifier;

    -- Event -> Identifier .{ Process }
    function Named_Block_Start return Natural is
    begin
        if Next = '{' then
            Shift; return Named_Block_Process - 1;
        else
            Error ("Expected { in named block start.");
            return 0;
        end if;
    end Named_Block_Start;

    -- Event -> { .Process }
    -- Process -> .
    -- Process -> .Event Process
    -- Event -> .Variable : = Expression ;
    -- Event -> .@ Identifier ;
    -- Event -> .% ( Expression ) ;
    -- Event -> .? ( Expression ) { Cases }
    -- Event -> .Identifier { Process }
    -- Event -> .{ Process }
    -- Event -> .Table_Header Table_Rows
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    -- Identifier -> .a More_Text
    -- Table_Header -> .$ Variable : : Columns x
    function Unnamed_Block return Natural is begin
        return Parse_Process (End_Unnamed_Block'Access);
    end Unnamed_Block;

    -- Event -> Table_Header .Table_Rows
    -- Table_Rows -> .
    -- Table_Rows -> .Table_Row Table_Rows
    -- Table_Row -> .x : Expression : : Columns x
    function First_Table_Row return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            case Kind (Top) is
                when Table_Row => return Gotos (More_Table_Rows);
                when Table_Rows => return Gotos (No_More_Table_Rows);
                when others =>
                    Error ("Couldn't reduce past " &
                        Symbol'Image (Kind (Top)) & " in a first table row.");
                    return 0;
            end case;
        end Gotos;
    begin
        case Next is
            when '$' | Character'First | '%' | '(' | ':' | '?' | '@' | '[' | '{'
                | '}' | '"' => return Gotos (Reduce (0, Table_Rows));
            when '!' => Shift; return Gotos (Start_Table_Row);
            when others =>
                if Ident_Char (Next) then
                    return Gotos (Reduce (0, Table_Rows));
                else
                    Error ("Didn't expect " & Next & " in a first table row.");
                    return 0;
                end if;
        end case;
    end First_Table_Row;

    -- Variable -> ( .Identifier )
    -- Identifier -> .a More_Text
    function Global_Identifier return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            case Kind (Top) is
                when Identifier => return Gotos (End_Global_Identifier);
                when others =>
                    Error ("Couldn't reduce past " &
                        Symbol'Image (Kind (Top)) & " in global identifier.");
                    return 0;
            end case;
        end Gotos;
    begin
        if Ident_Char (Next) then
            Shift; return Gotos (More_Identifier);
        else
            Error ("Didn't expect " & Next & " in a global identifier.");
            return 0;
        end if;
    end Global_Identifier;

    -- Variable -> [ .Identifier ]
    -- Identifier -> .a More_Text
    function Local_Identifier return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            case Kind (Top) is
                when Identifier => return Gotos (End_Local_Identifier);
                when others =>
                    Error ("Couldn't reduce past " &
                        Symbol'Image (Kind (Top)) & " in local identifier.");
                    return 0;
            end case;
        end Gotos;
    begin
        if Ident_Char (Next) then
            Shift; return Gotos (More_Identifier);
        else
            Error ("Didn't expect " & Next & " in a local identifier.");
            return 0;
        end if;
    end Local_Identifier;

    -- Identifier -> a .More_Text
    -- More_Text -> .
    -- More_Text -> .a More_Text
    function More_Identifier return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            if Kind (Top) = More_Text then
                return Gotos (End_Identifier);
            else
                Error ("Couldn't reduce past " &
                    Symbol'Image (Kind (Top)) & " in identifier.");
                return 0;
            end if;
        end Gotos;
    begin
        case Next is
            when ')' | ';' | ']' | '{' => return Gotos (Reduce (0, More_Text));
            when others =>
                if Ident_Char (Next) then
                    Shift; return Gotos (Even_More_Text);
                else
                    Error ("Didn't expect " & Next & " in an identifier.");
                    return 0;
                end if;
        end case;
    end More_Identifier;

    -- Table_Header -> $ .Variable : : Columns x
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function Table_Header_Variable return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            if Kind (Top) = Variable then
                return Gotos (Table_Header_Column_Sep);
            else
                Error ("Couldn't reduce past " &
                    Symbol'Image (Kind (Top)) & " in table header variable.");
                return 0;
            end if;
        end Gotos;
    begin
        case Next is
            when '(' => return Gotos (Global_Identifier);
            when ']' => return Gotos (Local_Identifier);
            when others =>
                Error ("Didn't expect " & Next &
                    " in a table header variable.");
                return 0;
        end case;
    end Table_Header;

    -- Process -> Event Process .
    function Done_More_Process return Natural is begin
        if Next = Character'First or Next = '}' then
            return Gotos (Reduce (2, Processed));
        else
            Error ("Didn't expect " & Next & " at the end of processes.");
            return 0;
        end if;
    end Done_More_Process;

    -- Event -> Variable : .= Expression ;
    function Assignment_Equals return Natural is begin
        if Next = '=' then
            Shift; return Assignment_Expression - 1;
        else
            Error ("Didn't expect " & Next &
                " in the middle of an assignment.");
            return 0;
        end if;
    end Assignment_Equals;

    -- Event -> @ Identifier .;
    function End_Call_Identifier return Natural is begin
        if Next = ';' then
            Shift; return End_Call - 1;
        else
            Error ("Didn't expect " & Next &
                " in the middle of an assignment.");
            return 0;
        end if;
    end End_Call_Identifier;

    -- Event -> % ( .Expression ) ;
    -- Expression -> .
    -- Expression -> .Expression_Part Expression
    -- Expression_Part -> .Variable
    -- Expression_Part -> .% ( Expression )
    -- Expression_Part -> .q String q
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function Execution_Event_Expression return Natural is begin
        return Parse_Expression (Close_Execution_Event'Access);
    end Execution_Event_Expression;

    -- Event -> ? ( .Expression ) { Cases }
    -- Expression -> .
    -- Expression -> .Expression_Part Expression
    -- Expression_Part -> .Variable
    -- Expression_Part -> .% ( Expression )
    -- Expression_Part -> .q String q
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function Condition_Identifier_Expression return Natural is begin
        return Parse_Expression (End_Condition_Identifier_Expression'Access);
    end Condition_Identifier_Expression;

    -- Event -> Identifier { .Process }
    -- Process -> .
    -- Process -> .Event Process
    -- Event -> .Variable : = Expression ;
    -- Event -> .@ Identifier ;
    -- Event -> .% ( Expression ) ;
    -- Event -> .? ( Expression ) { Cases }
    -- Event -> .Identifier { Process }
    -- Event -> .{ Process }
    -- Event -> .Table_Header Table_Rows
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    -- Identifier -> .a More_Text
    -- Table_Header -> .$ Variable : : Columns x
    function Named_Block_Process return Natural is begin
        return Parse_Process (End_Named_Process'Access);
    end Named_Block_Process;

    -- Event -> { Process .}
    function End_Block return Natural is begin
        if Next = '}' then
            return End_Unnamed_Process - 1;
        else
            Error ("Didn't expect " & Next & " at the end of a block.");
        end if;
    end End_Block;

    -- Event -> Table_Header Table_Rows .
    function No_More_Table_Rows return Natural is begin
        case Next is
            when '$' | Character'First | '%' | '(' | ':' | '?' | '@' | '[' |
                '{' | '}' | '"' => return Reduce (2, Event) - 1;
            when others =>
                if Ident_Char (Next) then
                    return Reduce (2, Event) - 1;
                else
                    Error ("Didn't expect " & Next & " after a table.");
                    return 0;
                end if;
        end case;
    end No_More_Table_Rows;

    -- Table_Rows -> Table_Row .Table_Rows
    -- Table_Rows -> .
    -- Table_Rows -> .Table_Row Table_Rows
    -- Table_Row -> .x : Expression : : Columns x
    function More_Table_Rows return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            case Kind (Top) is
                when Table_Row => return Gotos (More_Table_Rows);
                when Table_Rows => return Gotos (End_Table);
                when others =>
                    Error ("Couldn't reduce past " &
                        Symbol'Image (Kind (Top)) & " in more table rows.");
            end case;
        end Gotos;
    begin
        case Next is
            when '$' | Character'First | '%' | '(' | ':' | '?' | '@' | '[' |
                '{' | '}' | '"' => return Gotos (Reduce (0, Table_Rows));
            when '!' =>
                Shift; return Gotos (Start_Table_Row);
            when others =>
                if Ident_Char (Next) then
                    return Gotos (Reduce (0, Table_Rows));
                else
                    Error ("Didn't expect " & Next & " in more table rows.");
                    return 0;
                end if;
        end case;
    end More_Table_Rows;

    -- Table_Row -> x .: Expression : : Columns x
    function Start_Table_Row return Natural is begin
        if Next = ':' then
            return Table_Row_Value_Expression - 1;
        else
            Error ("Didn't expect " & Next & " when starting a table row.");
            return 0;
        end if;
    end Start_Table_Row;

    -- Variable -> ( Identifier .)
    function End_Global_Identifier return Natural is begin
        if Next = ')' then
            return End_Global_Variable - 1;
        else
            Error ("Didn't expect " & Next & " in a global variable id.");
            return 0;
        end if;
    end End_Global_Identifier;

    -- Variable -> [ Identifier .]
    function End_Local_Identifier return Natural is begin
        if Next = ']' then
            return End_Local_Variable - 1;
        else
            Error ("Didn't expect " & Next & " in a local variable id.");
            return 0;
        end if;
    end End_Local_Identifier;

    -- Identifier -> a More_Text .
    function End_Identifier return Natural is begin
        case Next is
            when ')' | ';' | ']' | '{' => return Reduce (2, Identifier) - 1;
            when others =>
                Error ("Didn't expect " & Next &
                    " at the end of an identifier.");
        end case;
    end End_Identifier;

    -- More_Text -> a .More_Text
    -- More_Text -> .
    -- More_Text -> .a More_Text
    function Even_More_Text return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            case Kind (Top) is
                when More_Text => return Gotos (No_More_Text);
                when others =>
                    Error ("Couldn't reduce past " &
                        Symbol'Image (Kind (Top)) & " in even more text.");
                    return 0;
            end case;
        end Gotos;
    begin
        case Next is
            when ')' | ';' | ']' | '(' => return Gotos (Reduce (0, More_Text));
            when others =>
                if Ident_Char (Next) then
                    Shift; return Gotos (Even_More_Text);
                else
                    Error ("Didn't expect " & Next & " in even more text.");
                end if;
        end case;
    end Even_More_Text;

    -- Table_Header -> $ Variable .: : Columns x
    function Table_Header_Column_Sep return Natural is begin
        if Next = ':' then
            Shift; return Table_Header_Column_Start - 1;
        end if;
    end Table_Header_Column_Sep;

    -- Event -> Variable : = .Expression ;
    -- Expression -> .
    -- Expression -> .Expression_Part Expression
    -- Expression_Part -> .Variable
    -- Expression_Part -> .% ( Expression )
    -- Expression_Part -> .q String q
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function Assignment_Expression return Natural is begin
        return Parse_Expression (End_Assignment_Expression'Access);
    end Assignment_Expression;

    -- Event -> @ Identifier ; .
    function End_Call return Natural is begin
        case Next is
            when ')' | ';' | '=' | ']' | '!' =>
                Error ("Didn't Expect " & Next & " after a call.");
                return 0;
            when others =>
                return Reduce (3, Event) - 1;
        end case;
    end End_Call;

    -- Event -> % ( Expression .) ;
    function Close_Execution_Event return Natural is begin
        if Next = ')' then
            Shift; return End_Execution_Event_Expression - 1;
        else
            Error ("Didn't expect " & Next & " after execution expression.");
            return 0;
        end if;
    end Close_Execution_Event;

    -- Expression -> Expression_Part .Expression
    -- Expression -> .
    -- Expression -> .Expression_Part Expression
    -- Expression_Part -> .Variable
    -- Expression_Part -> .% ( Expression )
    -- Expression_Part -> .q More_Text q
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function More_Expression return Natural is begin
        return Parse_Expression (No_More_Expression'Access);
    end More_Expression;

    -- Expression_Part -> Variable .
    function Variable_Expression_Part return Natural is begin
        case Next is
            when '%' | '(' | ')' | ':' | ';' | '[' | '"' =>
                return Reduce (1, Expression_Part) - 1;
            when others =>
                Error ("Didn't expect " & Next & " after variable part.");
                return 0;
        end case;
    end Variable_Expression_Part;

    -- Expression_Part -> % .( Expression )
    function Execution_Expression_Part_Start return Natural is begin
        if Next = '(' then
            Shift; return Start_Expression - 1;
        else
            Error ("Didn't expect " & Next & " between % and ( in execution.");
            return 0;
        end if;
    end Execution_Expression_Part_Start;

    -- Expression_Part -> q .String q
    -- String -> .
    -- String -> .s String
    function Text_Expression_Part_Start return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            if Kind (Top) = PString then
                return Gotos (End_Text);
            else
                Error ("Couldn't reduce " &
                    Symbol'Image (Kind (Top)) & " in text expression.");
                return 0;
            end if;
        end Gotos;
    begin
        if Next = '"' then
            return Gotos (Reduce (0, PString));
        elsif Next = '\' then
            Get_Next; -- Skip the \ but blindly shift afterwards
        end if;

        Shift; return Gotos (More_String);
    end Text_Expression_Part_Start;

    -- Event -> ? ( Expression .) { Cases }
    function End_Condition_Identifier_Expression return Natural is begin
        if Next = ')' then
            Shift; return Start_Cases - 1;
        else
            Error ("Didn't expect " & Next & " after condition expression.");
            return 0;
        end if;
    end End_Condition_Identifier_Expression;

    -- Event -> Identifier { Process .}
    function End_Named_Process return Natural is begin
        if Next = '}' then
            Shift; return End_Named_Block - 1;
        else
            Error ("Didn't expect " & Next & " after named block process.");
            return 0;
        end if;
    end End_Named_Process;

    -- Event -> { Process } .
    function End_Unnamed_Process return Natural is begin
        case Next is
            when ')' | ';' | '=' | ']' | '!' =>
                Error ("Didn't expect " & Next & " after process.");
                return 0;
            when others =>
                return Reduce (3, Event) - 1;
        end case;
    end End_Unnamed_Process;

    -- Table_Rows -> Table_Row Table_Rows .
    function End_Table return Natural is
        case Next is
            when ')' | ';' | '=' | ']' | '!' =>
                Error ("Didn't expect " & Next & " after table.");
                return 0;
            when others =>
                return Reduce (2, Table_Rows) - 1;
        end case;
    end End_Table;

    -- Table_Row -> x : .Expression : : Columns x
    -- Expression -> .
    -- Expression -> .Expression_Part Expression
    -- Expression_Part -> .Variable
    -- Expression_Part -> .% ( Expression )
    -- Expression_Part -> .q String q
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function Table_Row_Value_Expression return Natural is begin
        return Parse_Expression (Table_Row_Column_Sep);
    end Table_Row_Value_Expression;

    -- Variable -> ( Identifier ) .
    function End_Global_Variable return Natural is begin
        case Next is
            when '%' | '(' | ')' | ':' | ';' | '[' | '"' =>
                return Reduce (3, Variable) - 1;
            when others =>
                Error ("Didn't expect " & Next & " after global variable.");
                return 0;
        end case;
    end End_Global_Variable;

    -- Variable -> [ Identifier ] .
    function End_Local_Variable return Natural is begin
        case Next is
            when '%' | '(' | ')' | ':' | ';' | '[' | '"' =>
                return Reduce (3, Variable) - 1;
            when others =>
                Error ("Didn't expect " & Next & " after local variable.");
                return 0;
        end case;
    end End_Local_Variable;

    -- More_Text -> a More_Text .
    function No_More_Text return Natural is begin
        case Next is
            when ')' | ';' | ']' | '{' =>
                return Reduce (2, More_Text) - 1;
            when others =>
                Error ("Didn't expect " & Next & " after an identifier.");
                return 0;
        end case;
    end No_More_Text;

    -- Table_Header -> $ Variable : .: Columns x
    function Table_Header_Column_Start return Natural is begin
        if Next = ':' then
            Shift; return Table_Row_Column - 1;
        else
            Error ("Didn't expect " & Next & " between table header :s.");
            return 0;
        end if;
    end Table_Header_Column_Start;

    -- Event -> Variable : = Expression .;
    function End_Assignment_Expression return Natural is begin
        if Next = ';' then
            Shift; return End_Assignment - 1;
        else
            Error ("Assignments must be ; terminated.");
            return 0;
        end if;
    end End_Assignment_Expression;

    -- Event -> % ( Expression ) .;
    function End_Execution_Event_Expression return Natural is begin
        if Next = ';' then
            Shift; return End_Execution_Event - 1;
        else
            Error ("Executions must be ; terminated.");
            return 0;
        end if;
    end End_Execution_Event_Expression;

    -- Expression -> Expression_Part Expression .
    function No_More_Expression return Natural is begin
        if Next = ')' or Next = ':' or Next = ';' then
            return Reduce (2, Expression) - 1;
        else
            Error ("Didn't expect " & Next & " after an expression.");
            return 0;
        end if;
    end No_More_Expression;

    -- Expression_Part -> % ( .Expression )
    -- Expression -> .
    -- Expression -> .Expression_Part Expression
    -- Expression_Part -> .Variable
    -- Expression_Part -> .% ( Expression )
    -- Expression_Part -> .q String q
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function Start_Expression return Natural is begin
        return Parse_Expression (End_Execution_Expression'Access);
    end Start_Expression;

    -- Expression_Part -> q String .q
    function End_Text return Natural is begin
        if Next = '"' then
            Shift; return End_Text_Expression - 1;
        else
            Error ("Expected end of string.");
            return 0;
        end if;
    end End_Text;

    -- String -> s .String
    -- String -> .
    -- String -> .s String
    function More_String return Natural is
        function Gotos (Depth : Natural) return Natural is
            Top : Parse_Node;
        begin
            if Depth > 0 then
                return Depth - 1;
            end if;

            Output.Peek (Top);
            if Kind (Top) = PString then
                return Gotos (End_String);
            else
                Error ("Couldn't reduce past " &
                    Symbol'Image (Kind (Top)) & " in string.");
                return 0;
            end if;
        end Gotos;
    begin
        if Next = '"' then
            return Gotos (Reduce (0, PString));
        elsif Next = '\' then
            Get_Next; -- Skip the \ but blindly shift afterwards
        end if;

        Shift; return Gotos (More_String);
    end More_String;

    -- Event -> ? ( Expression ) .{ Cases }
    function Start_Cases return Natural is begin
        if Next = '{' then
            Shift; return Start_Case - 1;
        end if;
    end Start_Cases;

    -- Event -> Identifier { Process } .
    function End_Named_Block return Natural is begin
        case Next is
            when ')' | ';' | '=' | ']' | '!' =>
                Error ("Didn't expect " & Next & " after named block.");
                return 0;
            when others =>
                return Reduce (4, Event) - 1;
        end case;
    end End_Named_Block;

    -- Table_Row -> x : Expression .: : Columns x
    function Table_Row_Column_Sep return Natural is begin
        if Next = ':' then
            Shift; return Table_Row_Column_Start - 1;
        else
            Error ("Didn't expect " & Next & " after table row expression.");
            return 0;
        end if;
    end Table_Row_Column_Sep;

    -- Table_Header -> $ Variable : : .Columns x
    -- Columns -> .Expression : More_Columns
    -- Expression -> .
    -- Expression -> .Expression_Part Expression
    -- Expression_Part -> .Variable
    -- Expression_Part -> .% ( Expression )
    -- Expression_Part -> .q String q
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function Table_Row_Column return Natural is begin
        return Parse_Expression (
            Start_More_Columns'Access,
            Columnup => Table_Row_End'Access);
    end Table_Row_Column;

    -- Event -> Variable : = Expression ; .
    function End_Assignment return Natural is begin
        case Next is
            when ')' | ';' | '=' | ']' | '!' =>
                Error ("Didn't expect " & Next & " after an assignment.");
                return 0;
            when others =>
                return Reduce (5, Event) - 1;
        end case;
    end End_Assignment;

    -- Event -> % ( Expression ) ; .
    function End_Execution_Event return Natural is begin
        case Next is
            when ')' | ';' | '=' | ']' | '!' =>
                Error ("Didn't expect " & Next & " after an execution.");
                return 0;
            when others =>
                return Reduce (5, Event) - 1;
        end case;
    end End_Execution_Event;

    -- Expression_part -> % ( Expression .)
    function End_Execution_Expression return Natural is begin
        if Next = ')' then
            Shift; return End_Execution_Expression_Part - 1;
        else
            Error ("Didn't expect " & Next & " after execution expression.");
            return 0;
        end if;
    end End_Execution_Expression;

    -- Expression_Part -> q String q .
    function End_Text_Expression return Natural is begin
        case Next is
            when '%' | '(' | ')' | ':' | ';' | '[' | '"' =>
                return Reduce (3, Expression_Part) - 1;
            when others =>
                Error ("Didn't expect " & Next & " after string expression.");
                return 0;
        end case;
    end End_Text_Expression;

    -- String -> s String .
    function End_String return Natural is begin
        if Next = '"' then
            return Reduce (2, PString) - 1;
        else
            Error ("Expected "" to terminate string.");
            return 0;
        end if;
    end End_String;

    -- Event -> ? ( Expression ) { .Cases }
    -- Cases -> .
    -- Cases -> .Case Cases
    -- Case -> .Expression : Event
    -- Expression -> .
    -- Expression -> .Expression_Part Expression
    -- Expression_Part -> .Variable
    -- Expression_Part -> .% ( Expression )
    -- Expression_Part -> .q String q
    -- Variable -> .( Identifier )
    -- Variable -> .[ Identifier ]
    function Start_Case return Natural is begin
        return Parse_Expression (
            Case_Separator'Access,
            Caseup => More_Cases'Access,
            Casesup => End_Cases'Access);
    end Start_Case;

    -- Table_Row -> x : Expression : .: Columns x
    function Table_Row_Column_Start return Natural is begin
        if Next = ':' then
            Shift; return Start_Columns - 1;
        else
            Error ("Didn't expect " & Next & " between column starter.");
            return 0;
        end if;
    end Table_Row_Column_Start;

    -- Table_Header -> $ Variable : : Columns .x
    function Table_Row_End return Natural is begin
        if Next = '!' then
            Shift; return End_Table_Header - 1;
        else
            Error ("Didn't expect " & Next & " before header end.");
            return 0;
        end if;
    end Table_Row_End;

    -- Columns -> Expression .: More_Columns
    function Start_More_Columns return Natural is begin
        if Next = ':' then
            Shift; return Even_More_Columns - 1;
        else
            Error ("Didn't expect " & Next & " separating more columns.");
            return 0;
        end if;
    end Start_More_Columns;

    Ignored : Natural;
begin
    Get_Next;
    Ignored := Parse_Beginning;
end Do_Parse;
