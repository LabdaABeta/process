with Ada.Characters.Handling;
with Ada.Exceptions;

separate (Process.Grammar)
procedure Parse (
    From : access Character_Buffers.Buffer;
    Into : in out Grammar_Trees.Node) is
    use Grammar_Trees;

    Next : Character;
    Row : Natural := 0;
    Column : Natural := 0;

    procedure Fail (Msg : in String) is begin
        Ada.Exceptions.Raise_Exception (
            Parse_Error'Identity,
            "error at row" &
            Natural'Image (Row) &
            " col" &
            Natural'Image (Column) &
            ": " & Msg);
    end Fail;

    procedure Unexpected (During : in String) is begin
        Fail ("Didn't expect " & Next & " when parsing " & During);
    end Unexpected;

    procedure Get_Next is
        function Is_Whitespace (C : in Integer) return Integer;
        pragma Import (C, Is_Whitespace, "isspace");
    begin
        loop
            From.Remove (Next);

            if Next = Character'Val (10) then
                Row := Row + 1;
                Column := 0;
            else
                Column := Column + 1;
            end if;

            exit when Is_Whitespace (Character'Pos (Next)) = 0 or
                Next = Character'First;
        end loop;

        -- Skip comment lines
        while Next = '#' loop
            while Next /= Character'Val (10) loop
                From.Remove (Next);
            end loop;
            Row := Row + 1;
            Column := 0;
            From.Remove (Next);

            while Is_Whitespace (Character'Pos (Next)) /= 0 and Next /=
                Character'First loop
                From.Remove (Next);
            end loop;
        end loop;

    end Get_Next;

    function Accept_On (Expected : Character) return Boolean is begin
        if Next = Expected then
            Get_Next;
            return True;
        end if;

        return False;
    end Accept_On;

    procedure Expect (Expected : in Character; Into : in out Node) is begin
        if Accept_On (Expected) then
            Into.Append (Leaf ((Kind => Terminal, Data => Expected)));
        else
            if Expected = Character'First then
                Fail ("Expected end of input but found " & Next);
            elsif Next = Character'First then
                Fail ("Expected " & Expected & " but found end of input");
            else
                Fail ("Expected " & Expected & " but found " & Next);
            end if;
        end if;
    end Expect;

    procedure Parse_String (
        Start_On : in Character;
        End_On : in Character;
        Into : in out Node) is
        Added : Node := Create ((Kind => Identifier));
    begin
        if Next /= Start_On then
            Fail ("Expected " & Start_On);
        end if;

        Into.Append (Leaf ((Kind => Terminal, Data => Start_On)));
        Into.Append (Added);

        loop
            From.Remove (Next);

            exit when Next = End_On;

            if Next = '\' then
                From.Remove (Next);
                if Next /= End_On then
                    Added.Append (Leaf ((Kind => Terminal, Data => '\')));
                end if;
            end if;

            if Next = Character'First then
                Fail ("End of file before matching " & End_on);
            end if;

            Added.Append (Leaf ((Kind => Terminal, Data => Next)));
        end loop;

        Added.Cap;

        Into.Append (Leaf ((Kind => Terminal, Data => Next)));

        Get_Next;
    end Parse_String;

    procedure Parse_Program (Into : in out Node);
    procedure Parse_Event (Into : in out Node);
    procedure Parse_Single_Event (Into : in out Node);
    procedure Parse_Block_Event (Into : in out Node);
    procedure Parse_Assignment (Into : in out Node);
    procedure Parse_Call (Into : in out Node);
    procedure Parse_Condition (Into : in out Node);
    procedure Parse_Group (Into : in out Node);
    procedure Parse_Variable (Into : in out Node);
    procedure Parse_Expression (Into : in out Node);
    procedure Parse_Execution (Into : in out Node);
    procedure Parse_Branch (Into : in out Node);
    procedure Parse_Results (Into : in out Node);
    procedure Parse_Text (Into : in out Node);

    procedure Parse_Program (Into : in out Node) is
        Child : Node;
    begin
        loop
            case Next is
                when Character'First | '}' =>
                    exit;
                when '@' | '%' | '(' | '{' | '?' | '[' | '<' =>
                    Child := Create ((Kind => Event));
                    Into.Append (Child);
                    Parse_Event (Child);
                when others =>
                    Unexpected ("a program");
            end case;
        end loop;

        Into.Cap;
    end Parse_Program;

    procedure Parse_Event (Into : in out Node) is
        Child : Node;
    begin
        case Next is
            when '@' | '(' | '[' =>
                Child := Create ((Kind => Single_Event));
                Into.Append (Child);
                Parse_Single_Event (Child);
            when '<' | '?' | '{' | '%' =>
                Child := Create ((Kind => Block_Event));
                Into.Append (Child);
                Parse_Block_Event (Child);
            when others =>
                Unexpected ("an event");
        end case;

        Into.Cap;
    end Parse_Event;

    procedure Parse_Single_Event (Into : in out Node) is
        Child : Node;
    begin
        case Next is
            when '@' =>
                Child := Create ((Kind => Call));
                Into.Append (Child);
                Parse_Call (Child);
                Expect (';', Into);
            when '(' | '[' =>
                Child := Create ((Kind => Assignment));
                Into.Append (Child);
                Parse_Assignment (Child);
                Expect (';', Into);
            when others =>
                Unexpected ("a single event");
        end case;

        Into.Cap;
    end Parse_Single_Event;

    procedure Parse_Block_Event (Into : in out Node) is
        Child : Node;
    begin
        case Next is
            when '?' =>
                Child := Create ((Kind => Condition));
                Into.Append (Child);
                Parse_Condition (Child);
            when '<' | '{' | '%' =>
                Child := Create ((Kind => Group));
                Into.Append (Child);
                Parse_Group (Child);
            when others =>
                Unexpected ("a block event");
        end case;

        Into.Cap;
    end Parse_Block_Event;

    procedure Parse_Assignment (Into : in out Node) is
        LHS, RHS : Node;
    begin
        LHS := Create ((Kind => Variable));
        Into.Append (LHS);
        Parse_Variable (LHS);
        Expect ('=', Into);
        RHS := Create ((Kind => Expression));
        Into.Append (RHS);
        Parse_Expression (RHS);
        Into.Cap;
    end Parse_Assignment;

    procedure Parse_Call (Into : in out Node) is
    begin
        Expect ('@', Into);

        Parse_String ('<', '>', Into);

        Into.Cap;
    end Parse_Call;

    procedure Parse_Condition (Into : in out Node) is
        Check, Child : Node;
    begin
        Expect ('?', Into);
        Expect ('(', Into);
        Check := Create ((Kind => Expression));
        Into.Append (Check);
        Parse_Expression (Check);
        Expect (')', Into);
        Expect ('{', Into);

        while Next /= '}' loop
            Child := Create ((Kind => Branch));
            Into.Append (Child);
            Parse_Branch (Child);
        end loop;

        Expect ('}', Into);
        Into.Cap;
    end Parse_Condition;

    procedure Parse_Group (Into : in out Node) is
        Name : Node;

        procedure Parse_Contents is
            Contents : Node;
        begin
            Expect ('{', Into);
            Contents := Create ((Kind => Program));
            Into.Append (Contents);
            Parse_Program (Contents);
            Expect ('}', Into);
        end Parse_Contents;
    begin
        case Next is
            when '<' =>
                Parse_String ('<', '>', Into);
                Parse_Contents;
            when '%' =>
                Name := Create ((Kind => Execution));
                Into.Append (Name);
                Parse_Execution (Name);
                Parse_Contents;
            when '{' =>
                Parse_Contents;
            when others =>
                Unexpected ("a group");
        end case;

        Into.Cap;
    end Parse_Group;

    procedure Parse_Variable (Into : in out Node) is begin
        case Next is
            when '(' =>
                Parse_String ('(', ')', Into);
            when '[' =>
                Parse_String ('[', ']', Into);
            when others =>
                Unexpected ("a variable");
        end case;

        Into.Cap;
    end Parse_Variable;

    procedure Parse_Expression (Into : in out Node) is
        Child : Node;
    begin
        loop
            case Next is
                when '(' | '[' =>
                    Child := Create ((Kind => Variable));
                    Into.Append (Child);
                    Parse_Variable (Child);
                when '"' =>
                    Child := Create ((Kind => Text));
                    Into.Append (Child);
                    Parse_Text (Child);
                when '%' =>
                    Child := Create ((Kind => Execution));
                    Into.Append (Child);
                    Parse_Execution (Child);
                when ';' | ':' | ')' =>
                    exit;
                when others =>
                    Unexpected ("an expression");
            end case;
        end loop;

        Into.Cap;
    end Parse_Expression;

    procedure Parse_Execution (Into : in out Node) is
        Child : Node;
    begin
        Expect ('%', Into);
        Expect ('(', Into);
        Child := Create ((Kind => Expression));
        Into.Append (Child);
        Parse_Expression (Child);
        Expect (')', Into);
        Into.Cap;
    end Parse_Execution;

    procedure Parse_Branch (Into : in out Node) is
        LHS, RHS : Node;
    begin
        LHS := Create ((Kind => Results));
        Into.Append (LHS);
        Parse_Results (LHS);
        RHS := Create ((Kind => Event));
        Into.Append (RHS);
        Parse_Event (RHS);
        Into.Cap;
    end Parse_Branch;

    procedure Parse_Results (Into : in out Node) is
        Child : Node;
    begin
        loop
            case Next is
                when ':' =>
                    Expect (':', Into);
                    exit when Next = '(' or Next = '[' or Next = '@' or
                        Next = '%' or Next = '?' or Next = '<' or Next = '{';
                when '%' | '(' | '[' | '"' =>
                    Child := Create ((Kind => Expression));
                    Into.Append (Child);
                    Parse_Expression (Child);
                when others =>
                    Unexpected ("results");
            end case;
        end loop;

        Into.Cap;
    end Parse_Results;

    procedure Parse_Text (Into : in out Node) is begin
        Parse_String ('"', '"', Into);
        Into.Cap;
    end Parse_Text;
begin
    Get_Next;
    Parse_Program (Into);
    Into.Cap;
end Parse;
