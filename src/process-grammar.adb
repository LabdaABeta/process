with Ada.Exceptions;

package body Process.Grammar is
    procedure Parse (
        From : access Character_Buffers.Buffer;
        Into : in out Grammar_Trees.Node) is separate;

    procedure Parse (
        From : in Grammar_Trees.Node;
        Into : in out Processes.Node) is
        use type Grammar_Trees.Node;
        use type Processes.Node;

        procedure Unexpected (Kind : in Symbol; During : in String) is begin
            Ada.Exceptions.Raise_Exception (Parse_Error'Identity,
                "Didn't expect " & Symbol'Image (Kind) &
                " when parsing " & During);
        end Unexpected;

        function Extract_String (From : Grammar_Trees.Node) return String is
            function Extract (Out_Of : Grammar_Trees.Node) return String is
                Next : Grammar_Trees.Node;
            begin
                if Out_Of = null then
                    return "";
                else
                    Out_Of.Next (Next);
                    return Out_Of.Get.Data & Extract (Next);
                end if;
            end Extract;

            Data : Grammar_Trees.Node;
        begin
            From.First (Data);
            return Extract (Data);
        end Extract_String;

        procedure Build_Program (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        procedure Build_Event (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        procedure Build_Single_Event (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        procedure Build_Block_Event (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        procedure Build_Assignment (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        procedure Build_Call (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        procedure Build_Condition (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        procedure Build_Group (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        function Build_Variable (From : Grammar_Trees.Node)
            return Variable_Reference;
        function Build_Expression (From : Grammar_Trees.Node)
            return Process.Expression;
        procedure Build_Expression (
            From : in Grammar_Trees.Node;
            Into : in out Process.Expression);
        function Build_Execution (From : Grammar_Trees.Node)
            return Process.Expression;
        procedure Build_Branch (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node);
        function Build_Text (From : Grammar_Trees.Node)
            return String;
        function Build_Results (From : Grammar_Trees.Node)
            return Process.Expression;

        procedure Build_Program (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is
            Child : Grammar_Trees.Node;
        begin
            From.First (Child);
            while Child /= null loop
                Build_Event (Child, Into);
                Child.Next (Child);
            end loop;
        end Build_Program;

        procedure Build_Event (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is
            Child : Grammar_Trees.Node;
        begin
            From.First (Child);
            case Child.Get.Kind is
                when Single_Event =>
                    Build_Single_Event (Child, Into);
                when Block_Event =>
                    Build_Block_Event (Child, Into);
                when others =>
                    Unexpected (Child.Get.Kind, "an event");
            end case;
        end Build_Event;

        procedure Build_Single_Event (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is
            Child : Grammar_Trees.Node;
        begin
            From.First (Child);
            case Child.Get.Kind is
                when Assignment =>
                    Build_Assignment (Child, Into);
                when Call =>
                    Build_Call (Child, Into);
                when others =>
                    Unexpected (Child.Get.Kind, "a single event");
            end case;
        end Build_Single_Event;

        procedure Build_Block_Event (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is
            Child : Grammar_Trees.Node;
        begin
            From.First (Child);
            case Child.Get.Kind is
                when Condition =>
                    Build_Condition (Child, Into);
                when Group =>
                    Build_Group (Child, Into);
                when others =>
                    Unexpected (Child.Get.Kind, "a block event");
            end case;
        end Build_Block_Event;

        procedure Build_Assignment (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is
            Var, Equals, Expr : Grammar_Trees.Node;
        begin
            From.First (Var);
            Var.Next (Equals);
            Equals.Next (Expr);

            declare
                Target : Variable_Reference := Build_Variable (Var);
                Value : Process.Expression := Build_Expression (Expr);
            begin
                Into.Append (Processes.Leaf ((
                    Kind => Assignment_Event,
                    Size => Target.Size,
                    Target => Target,
                    Value => Value)));
            end;
        end Build_Assignment;

        procedure Build_Call (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is
            At_Term, Less_Term, Id, More_Term : Grammar_Trees.Node;
        begin
            From.First (At_Term);
            At_Term.Next (Less_Term);
            Less_Term.Next (Id);
            Id.Next (More_Term);

            declare
                Content : String := Extract_String (Id);
            begin
                Into.Append (Processes.Leaf ((
                    Kind => Call_Event,
                    Size => Content'Length,
                    Callee => Content)));
            end;
        end Build_Call;

        procedure Build_Condition (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is
            Ques, Open, Expr, Close, Beg, Branches : Grammar_Trees.Node;
            Inserted : Processes.Node;
        begin
            From.First (Ques);
            Ques.Next (Open);
            Open.Next (Expr);
            Inserted := Processes.Create ((
                Kind => Condition_Event,
                Size => 0,
                Condition => Build_Expression (Expr)));
            Into.Append (Inserted);
            Expr.Next (Close);
            Close.Next (Beg);
            Beg.Next (Branches);

            while Branches.Get.Kind = Branch loop
                Build_Branch (Branches, Inserted);
                Branches.Next (Branches);
            end loop;

            Inserted.Cap;
        end Build_Condition;

        procedure Build_Branch (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is
            Exprs, Event : Grammar_Trees.Node;
            Inserted : Processes.Node;
        begin
            From.First (Exprs);
            Inserted := Processes.Create ((
                Kind => Branch_Event,
                Size => 0,
                Result => Build_Results (Exprs)));
            Into.Append (Inserted);
            Exprs.Next (Event);
            Build_Event (Event, Inserted);

            Inserted.Cap;
        end Build_Branch;

        procedure Build_Group (
            From : in Grammar_Trees.Node;
            Into : in out Processes.Node) is

            procedure Unnamed (Out_Of : in Grammar_Trees.Node) is
                Prog : Grammar_Trees.Node;
                Inserted : Processes.Node := Processes.Create ((
                    Kind => Normal_Block,
                    Size => 0,
                    Name => ""));
            begin
                Into.Append (Inserted);
                Out_Of.Next (Prog);
                Build_Program (Prog, Inserted);
                Inserted.Cap;
            end Unnamed;

            procedure Named (Out_Of : in Grammar_Trees.Node) is
                Name, More, Open, Prog : Grammar_Trees.Node;
            begin
                Out_Of.Next (Name);
                Name.Next (More);
                More.Next (Open);
                Open.Next (Prog);

                declare
                    Id : String := Extract_String (Name);
                    Inserted : Processes.Node := Processes.Create ((
                        Kind => Normal_Block,
                        Size => Id'Length,
                        Name => Id));
                begin
                    Into.Append (Inserted);

                    Build_Program (Prog, Inserted);

                    Inserted.Cap;
                end;
            end Named;

            procedure Execute (Out_Of : in Grammar_Trees.Node) is
                Prog : Grammar_Trees.Node;
                Inserted : Processes.Node := Processes.Create ((
                    Kind => Execution_Block,
                    Size => 0,
                    Execution => Build_Execution (Out_Of)));
            begin
                Into.Append (Inserted);
                Out_Of.Next (Prog);
                Prog.Next (Prog); -- Skip the {
                Build_Program (Prog, Inserted);
                Inserted.Cap;
            end Execute;

            Child : Grammar_Trees.Node;
        begin
            From.First (Child);
            if Child.Get.Kind = Terminal then
                if Child.Get.Data = '{' then
                    Unnamed (Child);
                else
                    Named (Child);
                end if;
            else
                Execute (Child);
            end if;
        end Build_Group;

        function Build_Variable (From : Grammar_Trees.Node)
            return Variable_Reference is
            Start, Id : Grammar_Trees.Node;
        begin
            From.First (Start);
            Start.Next (Id);

            declare
                Contents : String := Extract_String (Id);
            begin
                return (
                    Size => Contents'Length,
                    Name => Contents,
                    Global => Start.Get.Data = '(');
            end;
        end Build_Variable;

        function Build_Expression (From : Grammar_Trees.Node)
            return Process.Expression is
            Result : Process.Expression := Expressions.Create ((Root_Value, 0));
        begin
            Build_Expression (From, Result);
            Result.Cap;
            return Result;
        end Build_Expression;

        procedure Build_Expression (
            From : in Grammar_Trees.Node;
            Into : in out Process.Expression) is
            Child : Grammar_Trees.Node;
        begin
            From.First (Child);

            while Child /= null loop
                case Child.Get.Kind is
                    when Variable =>
                        declare
                            Target : Variable_Reference :=
                                Build_Variable (Child);
                        begin
                            Into.Append (Expressions.Leaf ((
                                Kind => Variable_Value,
                                Size => Target.Size,
                                Target => Target)));
                        end;
                    when Text =>
                        declare
                            Text : String := Build_Text (Child);
                        begin
                            Into.Append (Expressions.Leaf ((
                                Kind => Text_Value,
                                Size => Text'Length,
                                Text => Text)));
                        end;
                    when Execution =>
                        Into.Append (Build_Execution (Child));
                    when others =>
                        Unexpected (Child.Get.Kind, "an expression");
                end case;
                Child.Next (Child);
            end loop;
        end Build_Expression;

        function Build_Execution (From : Grammar_Trees.Node)
            return Process.Expression is
            Result : Process.Expression := Expressions.Create ((Execution_Value, 0));
            Child : Grammar_Trees.Node;
        begin
            From.First (Child);
            Child.Next (Child); -- Skip %
            Child.Next (Child); -- Skip (
            Build_Expression (Child, Result);
            Result.Cap;
            return Result;
        end Build_Execution;

        function Build_Text (From : Grammar_Trees.Node)
            return String is
            Quote, Id : Grammar_Trees.Node;
        begin
            From.First (Quote);
            Quote.Next (Id);

            return Extract_String (Id);
        end Build_Text;

        function Build_Results (From : Grammar_Trees.Node)
            return Process.Expression is
            Result : Process.Expression := Expressions.Create ((Root_Value, 0));
            Child : Grammar_Trees.Node;
        begin
            From.First (Child);

            while Child /= null loop
                -- Skip the ':'s
                if Child.Get.Kind /= Terminal then
                    Result.Append (Build_Expression (Child));
                end if;
                Child.Next (Child);
            end loop;

            Result.Cap;
            return Result;
        end Build_Results;

    begin
        if From.Get.Kind /= Program then
            Ada.Exceptions.Raise_Exception (Parse_Error'Identity,
                "Can't parse partial trees.");
        end if;

        Build_Program (From, Into);
        Into.Cap;
    end Parse;
end Process.Grammar;
