with Ada.Containers;

package Graphviz is
    type Shape_Kind is (
        BOX, POLYGON, ELLIPSE, OVAL, CIRCLE, POINT, EGG, TRIANGLE, PLAINTEXT,
        PLAIN, DIAMOND, TRAPEZIUM, PARALLELOGRAM, HOUSE, PENTAGON, HEXAGON,
        SEPTAGON, OCTAGON, DOUBLECIRCLE, DOUBLEOCTAGON, TRIPLEOCTAGON,
        INVTRIANGLE, INVTRAPEZIUM, INVHOUSE, MDIAMOND, MSQUARE, MCIRCLE, RECT,
        RECTANGLE, SQUARE, STAR, NONE, UNDERLINE, CYLINDER, NOTE, TAB, FOLDER,
        BOX3D, COMPONENT, PROMOTER, CDS, TERMINATOR, UTR, PRIMERSITE,
        RESTRICTIONSITE, FIVEPOVERHANG, THREEPOVERHANG, NOVERHANG, ASSEMBLY,
        SIGNATURE, INSULATOR, RIBOSITE, RNASTAB, PROTEASESITE, PROTEINSTAB,
        RPROMOTER, RARROW, LARROW, LPROMOTER, RECORD_NODE);

    type Label_Kind is (TEXT_BASED, HTML_LIKE);

    type RGB_Component is mod 2**8;

    type HSV_Component is delta 0.001 range 0.0 .. 1.0;

    type Colour_Type is (RGB, HSV);

    type Colour (Kind : Colour_Type := RGB) is
        record
            case Kind is
                when RGB =>
                    R,G,B,A : RGB_Component;
                when HSV =>
                    H,S,V : HSV_Component;
            end case;
        end record;

    Colourless : constant Colour := (
        Kind => RGB, R => 0, G => 0, B => 0, A => 0);

    function Image (Item : Colour) return String;

    type Direction_Type is (FORWARD, BACK, BOTH, NONE);

    type Graph is private;

    function Hash (Item : Graph) return Ada.Containers.Hash_Type;

    function New_Graph return Graph;
    function New_Digraph return Graph;
    function New_Subgraph (Under : Graph) return Graph;
    function New_Cluster (Under : Graph) return Graph;

    -- Automatically sets foreground to be visible.
    -- Not applicable for top-level graphs.
    procedure Set_Colour (Item : in out Graph; To : in Colour);
    procedure Set_Label (Item : in out Graph; To : in String);
    procedure Set_Style (Item : in out Graph; To : in String);
    function Image (Item : Graph) return String;

    type Node is private;
    No_Node : constant Node;

    function Hash (Item : Node) return Ada.Containers.Hash_Type;

    function Create (Under : Graph) return Node;

    procedure Set_Colour (Item : in out Node; To : in Colour);
    procedure Set_Label (Item : in out Node; To : in String);
    procedure Set_Shape (Item : in out Node; To : in Shape_Kind);
    procedure Set_Label_Kind (Item : in out Node; To : in Label_Kind);
    procedure Set_Orientation (Item : in out Node; To : in Integer);
    function Get_Label (Item : Node) return String;
    function Get_Shape (Item : Node) return Shape_Kind;
    function Get_Parent (Item : Node) return Graph;
    function Get_Id (Item : Node) return String;
    function Image (Item : Node) return String;

    type Edge is private;

    function Hash (Item : Edge) return Ada.Containers.Hash_Type;

    function Create (From, To : Node) return Edge;

    procedure Set_Colour (Item : in out Edge; To : in Colour);
    procedure Set_Label (Item : in out Edge; To : in String);
    procedure Set_Dir (Item : in out Edge; To : in Direction_Type := FORWARD);
    procedure Set_From_Port (Item : in out Edge; To : in String);
    procedure Set_To_Port (Item : in out Edge; To : in String);
    procedure Set (Item : in Edge);
    function Image (Item : Edge) return String;
private
    type Graph_Data;
    type Node_Data;
    type Edge_Data;

    type Graph is access all Graph_Data;
    type Node is access all Node_Data;
    type Edge is access all Edge_Data;

    No_Node : constant Node := null;
end Graphviz;
